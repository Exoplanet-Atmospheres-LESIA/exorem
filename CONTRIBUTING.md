# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

This file describes the code convention used in the code, and the process to follow before pushing to the repository.

## Code convention

This code convention aims at improving the readability and the consistency of the code, a key for better efficiency, easier maintenance and modifications. Note that the code do not always respect all of these conventions, even if it should. If you can, help to improve the code by modifying where it is hard to read or to understand.

- While the code is mainly in Fortran, follow as much as possible the [PEP8](https://www.python.org/dev/peps/pep-0008/) convention style, however line must have a limit of 120 characters (instead of 79 in PEP8). End-of-line comments may go above this limit. Using modern IDEs like [PyCharm](https://www.jetbrains.com/pycharm/) can help a lot with that and is strongly recommended.
- Comments should help to describe what the code do, explicit intentions, or delimit subparts. Using comments for any other purpose, (e.g. ASCII art) is discouraged. Line comments should begin with an uppercase, end-of-line comments with a lowercase.
- Comments can also be used to mark future work to do, if beginning with `! TODO`.
- Inputs and outputs of the code must be in S.I. units, unless another standard is widely used by the community. If reasonably possible, S.I. units are preferred within the code over other standards.
- If a part of code is repeated multiple times, it should be in a function or a subroutine.
- Subroutines and functions should end with e.g. `end subroutine <name>`.
- Subroutines and functions arguments must start with inputs, and end with outputs.
- Input, output, and inout variables must be explicitly identified.
- Variables should be declared in this order:
    1. Arguments > local parameters > local variables
    2. Inputs > inouts > outputs
    3. Logicals > characters > integers > reals > doubles
    4. Alphanumerical order
- All subroutines and functions should have a complete docstring (following the [reST](https://thomas-cokelaer.info/tutorials/sphinx/docstring_python.html) convention), describing the purpose of the subroutine, and each argument, as well as their units if applicable. In Fortran, docstrings must begin and end with `! """`, while the lines in between begin with `!`:
```fortran
subroutine my_subroutine(temperature, energy, coefficient, entropy)
    ! """
    ! Do some stuff with the temperature.
    ! 
    ! :param temperature: (K) the temperature
    ! :param energy: (J) the energy
    ! :param coefficient: a dimensionless scalar
    ! :return: (J.K-1) somehow the entropy
    ! """
    use physics, only: cst_pi

    implicit none

    double precision, intent(in) :: coefficient
    double precision, intent(in) :: energy
    double precision, intent(in) :: temperature

    double precision, intent(out) :: entropy

    integer :: i  ! local variable

    i = 1
    entropy = energy / temperature * coefficient / (4d0 * cst_pi) + dble(i)
end subroutine my_subroutine
```
- Words in end of blocks or code keywords must be separated by spaces, like `double precision` or `end do`.
- All variables must be explicitly declared (`implicit none` is mandatory).
- Variables name should be as explicit as possible (e.g. `temperature` instead of `t`).
- Arrays names should use the plural if applicable (e.g. `temperatures`).
- Arrays should be dynamically allocated as much as possible. Hard-coded length arrays (e.g. `integer :: array(100)`) are strongly discouraged.
- For arrays in Fortran, the first dimension should be the fastest to change, the last dimension the slowest (for better performances):
```fortran
double precision, dimension(:, :, :), allocatable :: array

allocate(array(n_i, n_j, n_k))

do k = 1, n_k  ! slowest to change
    do j = 1, n_j
        do i = 1, n_i  ! fastest to change (even better if n_i > n_j > n_k)
            array(i, j, k) = <complicated equation>
        end do
    end do
end do
```
- Variables shared among processes should have a name ending with `__`.
- Keep optimization in mind.

## Push process

All personal modifications of the code (i.e. not intended to be used by most users) should be pushed to a new branch or a separate repository. All temporary or work-in-progress changes (i.e. alphas or betas) should be pushed to the dev branch or a new branch. Only pushes or merges to the master branch should change the version number. All pushes or merges to the master branch that change the code **must** change the version number.

1. Compile the code using i.e. `make exorem_debug`, ensure that there is no error nor warnings.
2. Add all new inputs into the inputs/example.nml file.
3. Execute e.g. `./exorem_degug.exe ../inputs/example.nml`, ensure that the run ends without error nor warnings. You should also ensure that the results are not too different from results from the previous version.
4. Increase the version numbers in src/exorem/exorem_interface.f90 (variable `exorem_version`) according to [Semantic Versioning](http://semver.org).
5. Add all notable changes into the CHANGELOG.
6. Modify the README if there is any change to the installation process or in the way to use the code.
7. Add all changes made to the input or output files into the wiki.
8. Update the distributed version of Exo-REM using `./sdist`.
9. Commit and push to the repository.
