"""
Functions to plot useful figures.
"""
import sys
import re

import matplotlib.pyplot as plt
from cycler import cycler

from src.python.condensations import *
import src.python.constants as cst
from src.python.interface import *


# Units
wavenumber_units = r'cm$^{-1}$'
wavelength_units = r'm'
spectral_radiosity_units = r'W$\cdot$m${-2}$/cm$^{-1}$'

# Colors
species_color = {
    'CH4': 'C7',
    'CO': 'C3',
    'CO2': 'C5',
    'FeH': 'C4',
    'H2O': 'C0',
    'H2S': 'olive',
    'HCN': 'darkblue',
    'K': 'C8',
    'Na': 'gold',
    'NH3': 'C9',
    'PH3': 'C1',
    'TiO': 'C2',
    'VO': 'darkgreen',
}

other_gases_color = {
    'Al': 'C7',
    'Ar': 'violet',
    'AsH3': 'm',
    'Ca': 'peru',
    'Co': 'aliceblue',
    'Cr': 'skyblue',
    'Cu': 'tan',
    'Fe': 'C4',
    'GeH4': 'olivedrab',
    'H': 'dimgray',
    'H2': 'k',
    'HCl': 'palegreen',
    'HF': 'y',
    'He': 'r',
    'KCl': 'darkolivegreen',
    'Kr': 'lightgrey',
    'Li': 'c',
    'Mg': 'darkorange',
    'Mn': 'olive',
    'N2': 'b',
    'NaCl': 'yellowgreen',
    'Ne': 'brown',
    'Ni': 'lightcoral',
    'P': 'wheat',
    'P2': 'navajowhite',
    'PH2': 'papayawhip',
    'PO': 'sandybrown',
    'SiH4': 'plum',
    'SiO': 'darkred',
    'Ti': 'lime',
    'TiO2': 'mediumseagreen',
    'V': 'forestgreen',
    'VO2': 'seagreen',
    'Xe': 'dodgerblue',
    'Zn': 'salmon'
}

cloud_color = {
    # condensation profiles
    'NH3': 'C9',
    'NH4SH': 'C1',
    'H2O': 'C0',
    'NH4Cl': 'C6',
    'H3PO4': 'wheat',
    'ZnS': 'C3',
    'KCl': 'C8',
    'Na2S': 'gold',
    'MnS': 'olive',
    'Cr': 'skyblue',
    'Cr2O3': 'deepskyblue',
    'MgSiO3': 'darkorange',
    'Mg2SiO4': 'C5',
    'SiO2': 'darkred',
    'TiN': 'lime',
    'VO': 'forestgreen',
    'Fe': 'C4',
    'CaTiO3': 'peru',
    'Al2O3': 'C7',
}


# Matplotlib sizes
TINY_FIGURE_FONT_SIZE = 40  # 0.5 text width 16/9
SMALL_FIGURE_FONT_SIZE = 22  # 0.25 text width
MEDIUM_FIGURE_FONT_SIZE = 16  # 0.5 text width
LARGE_FIGURE_FONT_SIZE = 22  # 1.0 text width

large_figsize = [19.20, 10.80]  # 1920 x 1080 for 100 dpi (default)


def update_figure_font_size(font_size):
    """
    Update the figure font size in a nice way.
    :param font_size: new font size
    """
    plt.rc('font', size=font_size)  # controls default text sizes
    plt.rc('axes', titlesize=font_size)  # fontsize of the axes title
    plt.rc('axes', labelsize=font_size)  # fontsize of the x and y labels
    plt.rc('axes.formatter', use_mathtext=True)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=font_size)  # fontsize of the tick labels
    plt.rc('xtick', direction='in')  # fontsize of the tick labels
    plt.rc('xtick.major', width=font_size / 10 * 0.8, size=font_size / 10 * 3.5)  # fontsize of the tick labels
    plt.rc('xtick.minor', width=font_size / 10 * 0.6, size=font_size / 10 * 2)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=font_size)  # fontsize of the tick labels
    plt.rc('ytick', direction='in')  # fontsize of the tick labels
    plt.rc('ytick.major', width=font_size / 10 * 0.8, size=font_size / 10 * 3.5)  # fontsize of the tick labels
    plt.rc('ytick.minor', width=font_size / 10 * 0.6, size=font_size / 10 * 2)  # fontsize of the tick labels
    plt.rc('legend', fontsize=font_size)  # legend fontsize
    plt.rc('figure', titlesize=font_size)  # fontsize of the figure title


# Utils
def get_species_string(string):
    """
    Get the string of a species from an Exo-REM data label.
    Example: volume_mixing_ratio_H2O -> H2O
    :param string: an Exo-REM data label
    :return: the species string
    """
    subscripts = re.findall(r'\d+', string)
    string = re.sub(r'\d+', '$_%s$', string)

    return string % tuple(subscripts)


# Plots
def plot_all(file_suffix, path_outputs, path_inputs='outputs/exorem/',
             wvn2wvl=True, pa2bar=False, legend=True, image_format='pdf', **kwargs):
    """
    Plot the most useful Exo-REM figures.
    :param file_suffix: suffix of the files
    :param path_outputs: figures directory
    :param path_inputs: path to the output files
    :param wvn2wvl: convert the x axis of spectrum figures from wavenumber (cm-1) to wavelength (m)
    :param pa2bar: convert the y axis of figures from Pa to bar
    :param legend: plot the legend on the figures
    :param image_format: format of the images
    """
    print(f'Generating {image_format} figures...')

    figure_name = file_suffix.rsplit('.')[0]

    plt.figure()
    update_figure_font_size(MEDIUM_FIGURE_FONT_SIZE)
    plt.rc('legend', fontsize=10)
    plot_emission_spectrum(path_inputs + file_suffix, wvn2wvl=wvn2wvl, **kwargs)
    plt.xscale('log')
    plt.tight_layout()
    plt.savefig(path_outputs + 'emission_spectrum_' + figure_name + '.' + image_format)
    plt.close('all')

    plot_transmission_spectrum(path_inputs + file_suffix, wvn2wvl=wvn2wvl, **kwargs)
    plt.xscale('log')
    plt.tight_layout()
    plt.savefig(path_outputs + 'transmission_spectrum_' + figure_name + '.' + image_format)
    plt.close('all')

    plot_emission_contribution_spectra(path_inputs + file_suffix,
                                       wvn2wvl=wvn2wvl, legend=legend, **kwargs)
    plt.xscale('log')
    plt.tight_layout()
    plt.savefig(path_outputs + 'contribution_emission_spectra_' + figure_name + '.' + image_format)
    plt.close('all')

    plot_transmission_contribution_spectra(path_inputs + file_suffix,
                                           wvn2wvl=wvn2wvl, legend=legend, **kwargs)
    plt.xscale('log')
    plt.tight_layout()
    plt.savefig(path_outputs + 'contribution_transmission_spectra_' + figure_name + '.' + image_format)
    plt.close('all')

    plot_vmr_profile(path_inputs + file_suffix, pa2bar=pa2bar, legend=legend, **kwargs)
    plt.tight_layout()
    plt.savefig(path_outputs + 'vmr_' + figure_name + '.' + image_format)
    plt.close('all')

    plot_temperature_profile(path_inputs + file_suffix, pa2bar=pa2bar, legend=legend, **kwargs)
    plt.tight_layout()
    plt.savefig(path_outputs + 'temperature_profile_' + figure_name + '.' + image_format)
    plt.close('all')

    print(f'Figures saved in directory \'{path_outputs}\'')


def plot_condensation_profiles(
        file, exclude=None, plot_h2o_triple_point=False, legend=False, bar2pa=False,
        markersize=1,  **kwargs
):
    """
    Plot the condensation profiles
    :param file: file containing the data
    :param exclude: list of the species to exclude
    :param plot_h2o_triple_point:
    :param legend: plot the legend
    :param bar2pa: convert the pressure axis from Pa to bar
    :param markersize: size of the marker
    """
    data = load_result(file)

    pressure = np.asarray(data['outputs']['layers']['pressure'])[::-1] * 1e-5  # Pa to bar

    meta = {}

    for key in data['model_parameters']['species']['elemental_abundances']:
        meta[key] = data['model_parameters']['species']['elemental_abundances'][key][()] / \
                           data['model_parameters']['species']['solar_elemental_abundances'][key][()]

    if exclude is None:
        exclude = [exclude]

    if 'H2O' in exclude:
        plot_h2o_triple_point = False

    # H2O
    try:
        condensation_temperature_h2o = get_h2o_saturation_temperature(file)
    except KeyError:
        condensation_temperature_h2o = np.zeros(np.size(pressure))

    # NH3
    try:
        condensation_temperature_nh3 = get_nh3_saturation_temperature(file)
    except KeyError:
        condensation_temperature_nh3 = np.zeros(np.size(pressure))

    # Others
    condensation_temperatures = {
        'NH3': condensation_temperature_nh3,
        'NH4SH': 10 ** 4 / (48.91 - 4.15 * np.log10(pressure) - 4.15 * np.log10(np.minimum(meta['N'], meta['S']))),
        'H2O': condensation_temperature_h2o,
        'NH4Cl': 10 ** 4 / (27.46 - 2.23 * (np.log10(pressure) + 0.33 * np.log10(np.minimum(meta['N'], meta['Cl'])))),
        # NH4Cl source: https://pubchem.ncbi.nlm.nih.gov/source/hsdb/483#section=Taste
        'ZnS': 10 ** 4 / (12.527 - 0.63 * np.log10(pressure) - 1.26 * np.log10(np.minimum(meta['Zn'], meta['S']))),
        'KCl': 10 ** 4 / (12.479 - 0.879 * np.log10(pressure) - 0.879 * np.log10(np.minimum(meta['K'], meta['Cl']))),
        'Na2S': 10 ** 4 / (10.045 - 0.72 * np.log10(pressure) - 0.5 * np.log10(np.minimum(meta['Na'], meta['S']))),
        'MnS': 10 ** 4 / (7.447 - 0.42 * np.log10(pressure) - 0.84 * np.log10(np.minimum(meta['Mn'], meta['S']))),
        'Cr': 10 ** 4 / (6.576 - 0.486 * np.log10(pressure) - 0.486 * np.log10(meta['Cr'])),
        'MgSiO3': 10 ** 4 / (6.26 - 0.35 * np.log10(pressure) - 0.7 * np.log10(np.minimum(meta['Mg'], meta['Si']))),
        'Mg2SiO4': 10 ** 4 / (5.89 - 0.37 * np.log10(pressure) - 0.73 * np.log10(np.minimum(meta['Mg'], meta['Si']))),
        'Fe': 10 ** 4 / (5.44 - 0.48 * np.log10(pressure) - 0.48 * np.log10(meta['Fe'])),
        'Al2O3': 10 ** 4 / (5.014 - 0.2179 * np.log10(pressure) - 0.58 * np.log10(meta['Al'])),
    }

    sorted_condensation_temperatures = sorted(condensation_temperatures.items(), key=lambda item: np.min(item[1]))

    if bar2pa:
        p_factor = 1e5
        pressure *= p_factor
        pressure_unit = 'Pa'
    else:
        p_factor = 1
        pressure_unit = 'bar'

    if plot_h2o_triple_point:
        pressure_h2o_triple_point = get_h2o_triple_point(pressure, condensation_temperature_h2o)

        p_melt = pressure[np.where(pressure > pressure_h2o_triple_point)]

        melting_temperature_ice1, melting_temperature_h2o = \
            get_h2o_melting_temperature(file, p_melt / p_factor)

        plt.semilogy([273.16], pressure_h2o_triple_point, color=species_color['H2O'], ls='', marker='o',
                     markersize=markersize)

        plt.semilogy(
            melting_temperature_h2o, p_melt,
            color=species_color['H2O'],
            **kwargs
        )

        melting_temperature_ice1 = np.append([273.16], melting_temperature_ice1)
        p_melt = np.append(pressure_h2o_triple_point, p_melt)

        plt.semilogy(
            melting_temperature_ice1, p_melt,
            color=species_color['H2O'],
            **kwargs
        )

    for condensation_temperature in sorted_condensation_temperatures:
        species_name = condensation_temperature[0]

        if any(species_name == np.asarray(exclude)):
            continue
        else:
            plt.semilogy(
                condensation_temperature[1], pressure,
                label=get_species_string(species_name), color=cloud_color[species_name], **kwargs
            )

    plt.ylim([np.max(pressure), np.min(pressure)])
    plt.xlabel('Temperature (K)')
    plt.ylabel(f'Pressure ({pressure_unit})')

    if legend:
        plt.legend()


def plot_emission_spectrum(file, wvn2wvl=False, cm2um=False, solid_angle=4*cst.pi, cloud_coverage=None,
                           xlim=None, legend=False, **kwargs):
    """
    Plot the emission spectrum.
    :param file: spectrum file
    :param wvn2wvl: convert wavenumbers (cm-1) into wavelengths (m)
    :param cm2um: if True, convert the units from W.m-2/cm-1 to W.m-2/um
    :param solid_angle: solid angle view of the object
    :param cloud_coverage: cloud coverage, between 0 (no cloud) and 1 (full coverage)
    :param legend: plot the legend
    :param xlim: x-axis limits (i.e. [min, max])
    :param kwargs: keyword arguments for plot
    """
    data_dict = load_result(file)

    if wvn2wvl:
        x_axis = cst.wavenumber2wavelength(np.asarray(data_dict['outputs']['spectra']['wavenumber']))
        x_label = rf'Wavelength ({wavelength_units})'
    else:
        x_axis = np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        x_label = rf'Wavenumber ({wavenumber_units})'

    if cloud_coverage is not None:
        radiosity_full = np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity_cloud'])
        radiosity_clear = np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity_clear'])
        y_axis = (1 - cloud_coverage) * radiosity_clear + cloud_coverage * radiosity_full
    else:
        y_axis = np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity'])

    if cm2um:
        y_axis = cst.radiosity_cm2radiosity_um(
            y_axis,
            np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        )

    plt.plot(x_axis, y_axis * solid_angle / (4 * cst.pi), **kwargs)

    plt.gca().ticklabel_format(useMathText=True)
    plt.xlabel(x_label)

    if cm2um:
        units = spectral_radiosity_units
        units = units.rsplit('/', 1)[0]
        units += '/um'
    else:
        units = spectral_radiosity_units

    plt.ylabel(f'Spectral radiosity ({units})')

    if xlim is None:
        plt.xlim([np.min(x_axis), np.max(x_axis)])
    else:
        plt.xlim(xlim)

    plt.ylim([0, None])

    if legend:
        plt.legend()


def plot_emission_contribution_spectra(
        file, wvn2wvl=False, cm2um=False, solid_angle=4*cst.pi,
        xlim=None, legend=False, exclude=None, **kwargs
):
    """
    Plot the different contributions of the emission spectrum.
    :param file: spectrum file
    :param wvn2wvl: convert wavenumbers (cm-1) into wavelengths (m)
    :param cm2um: if True, convert the units from W.m-2/cm-1 to W.m-2/um
    :param solid_angle: solid angle view of the object
    :param xlim: x-axis limits (i.e. [min, max])
    :param legend: plot the legend
    :param exclude: list of label to exclude (e.g. ['H2O', 'clouds'])
    :param kwargs: keyword arguments for plot
    """
    if exclude is None:
        exclude = np.array([None])
    else:
        exclude = np.asarray(exclude)

    data_dict = load_result(file)

    if wvn2wvl:
        x_axis = 1e-2 / np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        x_axis_label = r'Wavelength ($\mu$m)'
    else:
        x_axis = np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        x_axis_label = r'Wavenumber (cm$^{-1}$)'

    for key in data_dict['outputs']['spectra']['emission']['contributions']:
        if key == 'thermal' or key == 'cia_rayleigh' or key == 'clouds':
            continue

        if np.any(exclude == key):
            continue

        color = None

        for species in species_color:
            if species == key:
                color = species_color[species]
                break

        if cm2um:
            y_axis = cst.radiosity_cm2radiosity_um(
                np.asarray(data_dict['outputs']['spectra']['emission']['contributions'][key]),
                np.asarray(data_dict['outputs']['spectra']['wavenumber'])
            )
        else:
            y_axis = np.asarray(data_dict['outputs']['spectra']['emission']['contributions'][key])

        label = get_species_string(key)

        if np.size(y_axis) > 0:
            plt.plot(x_axis, y_axis * solid_angle / (4 * cst.pi),
                     color=color, label=label, **kwargs)

    if 'clouds' not in exclude:
        if cm2um:
            y_axis = cst.radiosity_cm2radiosity_um(
                np.asarray(data_dict['outputs']['spectra']['emission']['contributions']['clouds']),
                np.asarray(data_dict['outputs']['spectra']['wavenumber'])
            )
        else:
            y_axis = np.asarray(data_dict['outputs']['spectra']['emission']['contributions']['clouds'])

        if np.size(y_axis) > 0:
            plt.plot(x_axis, y_axis * solid_angle / (4 * cst.pi), color='k', ls='--', label='clouds')

    if 'cia' not in exclude:
        if cm2um:
            y_axis = cst.radiosity_cm2radiosity_um(
                np.asarray(data_dict['outputs']['spectra']['emission']['contributions']['cia_rayleigh']),
                np.asarray(data_dict['outputs']['spectra']['wavenumber'])
            )
        else:
            y_axis = np.asarray(data_dict['outputs']['spectra']['emission']['contributions']['cia_rayleigh'])

        if np.size(y_axis) > 0:
            plt.plot(x_axis, y_axis * solid_angle / (4 * cst.pi), color='k', ls=':', label='CIA+Ray')

    if cm2um:
        y_axis = cst.radiosity_cm2radiosity_um(
            np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity']),
            np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        )
    else:
        y_axis = np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity'])

    plt.plot(x_axis, y_axis * solid_angle / (4 * cst.pi), color='k', label='Total', **kwargs)

    plt.gca().ticklabel_format(useMathText=True)

    if xlim is None:
        plt.xlim([np.min(x_axis), np.max(x_axis)])
    else:
        plt.xlim(xlim)

    plt.ylim([0, None])
    plt.xlabel(x_axis_label)

    if cm2um:
        units = spectral_radiosity_units
        units = units.rsplit('/', 1)[0]
        units += '/um'
    else:
        units = spectral_radiosity_units

    plt.ylabel(f'Spectral radiosity ({units})')

    if legend:
        plt.legend()


def plot_kernel(file, cmap='inferno', vmin=None, vmax=None):
    """
    Plot an Exo-REM kernel.
    :param file: file containing the kernel
    :param cmap: colormap to use
    :param vmin: minimal value for the colorbar
    :param vmax: maximal value for the colorbar
    :return: the kernel data
    """
    data = load_result(file)
    pressures = np.asarray(data['outputs']['levels']['pressure'])
    kernel = np.asarray(data['outputs']['levels']['kernel_temperature'])

    k_s = np.sign(kernel)

    wh = np.where(np.abs(kernel) < sys.float_info.min)

    kernel[wh] = np.nan
    k_p = np.log10(np.abs(kernel))

    plt.pcolormesh(pressures, pressures, k_p * k_s, cmap=cmap, vmin=vmin, vmax=vmax, shading='auto')
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.colorbar()

    return kernel


def plot_temperature_profile(
        file, pa2bar=False,
        plot_convective_layer=True, plot_photosphere=True, plot_condensation=True,
        photosphere_wavelength_range=None, exclude=None, legend=True,
        color='k', ls_condensation=':', linewidth=2,
        **kwargs
):
    """
    Plot a model temperature profile.
    :param file: file containing the data
    :param pa2bar: convert Pascals into bars
    :param plot_convective_layer: plot the convective layers
    :param plot_photosphere: plot the photosphere from the model spectrum file
    :param plot_condensation: plot the condensation profiles
    :param photosphere_wavelength_range: set the wavelength range of the photosphere (e.g. [1e-6, 5e-6])
    :param exclude: list of condensation profiles to exclude (e.g. ['H2O', 'NH3'])
    :param legend: plot the legend
    :param color: color of the temperature profile
    :param ls_condensation: linestyle of the condensation profiles
    :param linewidth: linewidth of the temperature profile
    :param kwargs: keyword arguments for plot
    :return:
    """
    data_dict = load_result(file)

    pressure = np.asarray(data_dict['outputs']['levels']['pressure'])
    temperature = np.asarray(data_dict['outputs']['levels']['temperature'])

    # plot cloud condensation curves
    if plot_condensation:
        plot_condensation_profiles(
            file,  # pressure Pa to bar
            plot_h2o_triple_point=True, legend=legend, bar2pa=not pa2bar, exclude=exclude, ls=ls_condensation,
            linewidth=linewidth, markersize=linewidth * 4
        )

    if pa2bar:
        pressure *= 1e-5
        p_unit = 'bar'
    else:
        p_unit = 'Pa'

    # Convective layer
    if plot_convective_layer:
        convective_temperature = np.zeros(len(pressure))

        for i in range(len(pressure)):
            if np.asarray(data_dict['outputs']['levels']['is_convective'])[i]:
                convective_temperature[i] = temperature[i]
            else:
                convective_temperature[i] = np.nan

        plt.semilogy(convective_temperature, pressure, 'r', linewidth=2 * linewidth)

    # Temperature profile
    plt.semilogy(temperature, pressure, linewidth=linewidth, color=color, **kwargs)

    plt.ylim([np.max(pressure), np.min(pressure)])
    plt.xlabel('Temperature (K)')
    plt.ylabel(f'Pressure ({p_unit})')

    # Photosphere
    if plot_photosphere:
        if photosphere_wavelength_range is None:
            photosphere_wavelength_range = [5e-6, 25e-6]

        wavenumber = np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        flux = np.asarray(data_dict['outputs']['spectra']['emission']['spectral_radiosity']) * wavenumber[:] ** 2 * 1e-4

        wavelength = 1.0e-2 / wavenumber[:]  # cm-1 to m
        brightness_temperature = np.zeros(len(wavelength))

        for i in range(len(wavelength)):
            brightness_temperature[i] = cst.h * cst.c / cst.Boltzmann / wavelength[i] / \
                                        np.log(
                                            1 + 2 * cst.h * cst.c ** 2 / wavelength[i] ** 5 / (1e6 * flux[i] / np.pi))

        brightness_temperature = brightness_temperature[
            np.where(
                np.logical_and(
                    wavelength[:] >= photosphere_wavelength_range[0],
                    wavelength[:] <= photosphere_wavelength_range[1],
                )
            )
        ]

        pressure_min = pressure[np.where(temperature == min(temperature))]

        temperature_photosphere = temperature[
            np.where(
                np.logical_and(
                    pressure >= pressure_min,
                    temperature >= min(brightness_temperature)
                )
            )[0]
        ]
        pressure_photosphere = pressure[
            np.where(
                np.logical_and(
                    pressure >= pressure_min,
                    temperature >= min(brightness_temperature)
                )
            )
        ]

        pressure_photosphere = pressure_photosphere[temperature_photosphere[:] <= max(brightness_temperature)]
        temperature_photosphere = temperature_photosphere[temperature_photosphere[:] <= max(brightness_temperature)]

        plt.semilogy(temperature_photosphere, pressure_photosphere, 'orange', linewidth=linewidth)


def plot_transmission_spectrum(file, offset=0.0, cloud_coverage=None, wvn2wvl=False,
                               xlim=None, **kwargs):
    """
    Plot a transmission spectrum.
    :param file: spectrum file
    :param offset: (m) altitude offset of the transmission spectrum
    :param cloud_coverage: cloud coverage, between 0 (no cloud) and 1 (full coverage)
    :param wvn2wvl: convert wavenumbers (cm-1) into wavelengths (m)
    :param xlim: x-axis limits (i.e. [min, max])
    :param kwargs: keyword arguments for plot
    """
    data_dict = load_result(file)

    try:
        x_axis = np.asarray(data_dict['outputs']['spectra']['wavenumber'])
        y_axis = np.asarray(data_dict['outputs']['spectra']['transmission']['transit_depth'])
        x_err = None
        y_err = None

        if wvn2wvl:
            x_axis = 1e-2 / x_axis

        star_radius = data_dict['model_parameters']['light_source']['radius'][()]
        planet_radius_0 = star_radius * np.sqrt(y_axis)

        if cloud_coverage is not None:
            planet_radius_full = \
                star_radius * np.sqrt(
                    np.asarray(data_dict['outputs']['spectra']['transmission']['transit_depth_full_cover']))
            planet_radius_clear = \
                star_radius * np.sqrt(
                    np.asarray(data_dict['outputs']['spectra']['transmission']['transit_depth_clear']))
            planet_radius_0 = (1 - cloud_coverage) * planet_radius_clear + cloud_coverage * planet_radius_full

        y_axis = ((planet_radius_0 + offset) / star_radius) ** 2

        y_axis *= 1e6  # unit to ppm
    except KeyError:
        try:
            x_axis = data_dict['wavelength'] * 1e-6  # um to m
            x_err = None

            if not wvn2wvl:
                x_axis = 1e-2 / x_axis

        except KeyError:
            x_axis = np.mean([data_dict['wavelength_min'], data_dict['wavelength_max']], axis=0) * 1e-6

            if not wvn2wvl:
                x_err = [1e-2 / x_axis - 1e4 / data_dict['wavelength_max'],
                         1e4 / data_dict['wavelength_min'] - 1e-2 / x_axis]  # um to cm-1
                x_axis = 1e-2 / x_axis
            else:
                x_err = (data_dict['wavelength_max'] - data_dict['wavelength_min']) / 2 * 1e-6  # um to m

        y_axis = data_dict['transit_depth']

        try:
            y_err = data_dict['transit_depth_uncertainty']
        except KeyError:
            y_err = np.asarray([data_dict['transit_depth_uncertainty_down'], data_dict['transit_depth_uncertainty_up']])

    if wvn2wvl:
        x_label = rf'Wavelength ({wavelength_units})'
    else:
        x_label = rf'Wavenumber ({wavenumber_units})'

    y_label = 'Transit depth (ppm)'

    plt.errorbar(x_axis, y_axis, xerr=x_err, yerr=y_err, **kwargs)

    plt.gca().ticklabel_format(useMathText=True)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    if xlim is None:
        plt.xlim([np.min(x_axis), np.max(x_axis)])
    else:
        plt.xlim(xlim)

    plt.ylim([None, None])


def plot_transmission_contribution_spectra(file, offset=0.0, cloud_altitude=None, wvn2wvl=False,
                                           xlim=None, legend=False, exclude=None,
                                           **kwargs):
    """
    Plot the different contributions in the transmission spectrum.
    :param file: spectrum file
    :param offset: (m) altitude offset of the transmission spectrum
    :param cloud_altitude: (m) add an opaque cloud deck at the given altitude
    :param wvn2wvl: convert wavenumbers (cm-1) into wavelengths (m)
    :param xlim: x-axis boundaries
    :param legend: plot the legend
    :param exclude: list of label to exclude (e.g. ['H2O', 'clouds'])
    :param kwargs: keyword arguments for plot
    """
    if exclude is None:
        exclude = np.array([None])
    else:
        exclude = np.asarray(exclude)

    data_dict = load_result(file)

    x_axis = np.asarray(data_dict['outputs']['spectra']['wavenumber'])

    if wvn2wvl:
        x_axis = 1e-2 / x_axis

    if wvn2wvl:
        x_axis_label = rf'Wavelength ({wavelength_units})'
    else:
        x_axis_label = rf'Wavenumber ({wavenumber_units})'

    for key in data_dict['outputs']['spectra']['transmission']['contributions']:
        if key == 'cia_rayleigh' or key == 'clouds':
            continue

        color = None

        for species in species_color:
            if species == key:
                color = species_color[species]
                break

        label = key

        if np.any(exclude == label):
            continue

        label = get_species_string(label)

        y_axis = np.asarray(data_dict['outputs']['spectra']['transmission']['contributions'][key])

        if offset != 0:
            star_radius = data_dict['model_parameters']['light_source']['radius'][()]
            planet_radius_0 = star_radius * np.sqrt(y_axis)
            y_axis = ((planet_radius_0 + offset) / star_radius) ** 2

        plt.plot(x_axis, y_axis * 1e6, color=color, label=label, **kwargs)

    if cloud_altitude is not None:
        star_radius = data_dict['model_parameters']['light_source']['radius'][()]
        planet_radius = data_dict['model_parameters']['target']['radius_1e5Pa'][()]

        planet_radius_0 = planet_radius + cloud_altitude
        y_axis = np.ones(np.size(x_axis)) * ((planet_radius_0 + offset) / star_radius) ** 2

        plt.plot(x_axis, y_axis * 1e6, color='k', ls='--', label='cloud')
    elif 'clouds' not in exclude:
        y_axis = np.asarray(data_dict['outputs']['spectra']['transmission']['contributions']['clouds'])

        if offset != 0:
            star_radius = data_dict['model_parameters']['light_source']['radius'][()]
            planet_radius_0 = star_radius * np.sqrt(y_axis)
            y_axis = ((planet_radius_0 + offset) / star_radius) ** 2

        plt.plot(x_axis, y_axis * 1e6, color='k', ls='--', label='clouds')

    if 'cia' not in exclude:
        y_axis = np.asarray(data_dict['outputs']['spectra']['transmission']['contributions']['cia_rayleigh'])

        if offset != 0:
            star_radius = data_dict['model_parameters']['light_source']['radius'][()]
            planet_radius_0 = star_radius * np.sqrt(y_axis)
            y_axis = ((planet_radius_0 + offset) / star_radius) ** 2

        plt.plot(x_axis, y_axis * 1e6, color='k', ls=':', label='CIA+Ray')

    y_axis = np.asarray(data_dict['outputs']['spectra']['transmission']['transit_depth'])

    if 'Total' not in exclude:
        if offset != 0:
            star_radius = data_dict['model_parameters']['light_source']['radius'][()]
            planet_radius_0 = star_radius * np.sqrt(y_axis)
            y_axis = ((planet_radius_0 + offset) / star_radius) ** 2

        plt.plot(x_axis, y_axis * 1e6, color='k', label='Total', **kwargs)

    plt.gca().ticklabel_format(useMathText=True)

    if xlim is None:
        plt.xlim([np.min(x_axis), np.max(x_axis)])
    else:
        plt.xlim(xlim)

    plt.ylim([None, None])
    plt.xlabel(x_axis_label)
    plt.ylabel(f'Transit depth (ppm)')

    if legend:
        plt.legend()


def plot_transmission_spectrum_derivative(file, wvn2wvl=False, cmap='inferno',
                                          ppu2ppm=False, **kwargs):
    """
    Plot the derivative of the transmission spectrum.
    :param file: file containing the derivative
    :param wvn2wvl: convert wavenumbers (cm-1) into wavelegths (m)
    :param cmap: colormap to use
    :param ppu2ppm: convert part per units into part per millions
    :param kwargs: keyword arguments for pcolormesh
    :return: the derivative data
    """
    data = load_result(file)
    pressures = np.asarray(data['outputs']['levels']['pressure'])
    wavenumbers = np.asarray(data['outputs']['spectra']['wavenumbers'])
    kernel = np.asarray(data['outputs']['spectra']['transmission']['derivative'])

    kernel = np.transpose(kernel)

    if ppu2ppm:
        kernel *= 1e6
        deriv_unit = ' (ppm)'
    else:
        deriv_unit = ''

    deriv_str = r'$\mathcal{T}$'

    if wvn2wvl:
        wavenumbers = 1e-2 / wavenumbers
        x_lbl = r'Wavelength (m)'
    else:
        x_lbl = r'Wavenumber (cm${-1}$)'

    plt.pcolormesh(wavenumbers, pressures, kernel, cmap=cmap, shading='auto', **kwargs)
    plt.xlim([np.min(wavenumbers), np.max(wavenumbers)])
    plt.ylim([np.max(pressures), np.min(pressures)])
    plt.semilogy()
    plt.xlabel(x_lbl)
    plt.ylabel('Pressure (Pa)')
    plt.colorbar(label=deriv_str + deriv_unit)

    return kernel


def plot_vmr_profile(
        file, plot_other_gases=None, plot_elements=None, plot_saturation=None,
        ls_other=':', color_other=None, ls_elements='-.', color_elements=None,
        ls_saturation='--', color_saturation=None,
        legend=False, pa2bar=False, exclude=None,
        xlim=None, ylim=None, **kwargs
):
    """
    Plot the volume mixing ratio profiles of a model
    :param file: vmr file
    :param plot_other_gases: list of non-absorbant gases to plot (e.g. ['H2, He'])
    :param plot_elements: list of gaseous elemental abundance to plot (e.g. ['N', 'O'])
    :param plot_saturation: list of saturation profiles to plot (e.g. ['Mg'])
    :param ls_other: linestyle of the non-absorbant gases
    :param color_other: list of color of the non-absorbant gases (e.g. ['C0', 'red'])
    :param ls_elements: linestyle of element abundance profiles
    :param color_elements: list of color of the element abundance profiles
    :param ls_saturation: linestyle of the saturation profiles
    :param color_saturation: color of the saturation profiles
    :param legend: plot the legend
    :param pa2bar: convert Pascals into bars
    :param exclude: list of absorbant gases to exclude
    :param xlim: x-axis limits ([min, max])
    :param ylim: y-axis limits ([min, max])
    :param kwargs: keyword arguments for plot
    """
    if exclude is None:
        exclude = [None]

    data = load_result(file)

    # Other gases
    if plot_other_gases is None:
        plot_other_gases = [None]
    elif plot_other_gases == 'all':
        plot_other_gases = list(data['outputs']['layers']['volume_mixing_ratios']['gases'])

    if color_other is None:
        color_other = {}

        for g in plot_other_gases:
            try:
                color_other[g] = other_gases_color[g]
            except KeyError:
                color_other[g] = 'k'

    # Saturation
    if plot_saturation is None:
        plot_saturation = [None]
    elif plot_saturation == 'all':
        plot_saturation = list(data['outputs']['layers']['condensates']['volume_mixing_ratio_saturation'])

    if color_saturation is None:
        color_saturation = {}

        for s in cloud_color:
            try:
                color_saturation[s] = cloud_color[s]
            except KeyError:
                color_saturation[s] = 'k'

    # Elements
    if plot_elements is None:
        plot_elements = [None]
    elif plot_elements == 'all':
        plot_elements = list(data['outputs']['layers']['volume_mixing_ratios']['elements_gas_phase'])

    if color_elements is None:
        color_elements = plt.rcParams['axes.prop_cycle'].by_key()['color']

    color_elements = (cycler(color=color_elements))

    colors = species_color

    if pa2bar:
        pressure = np.asarray(data['outputs']['layers']['pressure']) * 1e-5
        p_unit = 'bar'
    else:
        pressure = np.asarray(data['outputs']['layers']['pressure'])
        p_unit = 'Pa'

    # Init
    p_lim = [np.max(pressure), np.min(pressure)]

    plt.loglog()

    # Plot
    for key in data['outputs']['layers']['volume_mixing_ratios']['absorbers']:
        key_species = key

        if key_species in exclude:
            continue

        for species in species_color:
            if species == key_species:
                label = get_species_string(species)

                vmr = np.asarray(data['outputs']['layers']['volume_mixing_ratios']['absorbers'][key])
                plt.loglog(vmr, pressure, color=colors[species], label=label, **kwargs)
                break

    for key in plot_other_gases:
        key_species = key

        if key_species is None:
            continue

        label = get_species_string(key_species)
        color = 'k'

        for species in species_color:
            if species == key_species:
                color = colors[key_species]
                break
            else:
                color = color_other[key_species]

        vmr = np.asarray(data['outputs']['layers']['volume_mixing_ratios']['gases'][key])
        plt.loglog(vmr, pressure, label=label, ls=ls_other, color=color)

    for key in plot_saturation:
        key_species = key

        if key_species is None:
            continue

        label = get_species_string(key_species)

        vmr = np.asarray(data['outputs']['layers']['condensates']['volume_mixing_ratio_saturation'][key])
        plt.loglog(vmr, pressure, label=label, ls=ls_saturation, color=color_saturation[key_species])

    plt.gca().set_prop_cycle(color_elements)

    for key in plot_elements:
        key_species = key

        if key_species is None:
            continue

        label = key_species

        vmr = np.asarray(data['outputs']['layers']['volume_mixing_ratios']['elements_gas_phase'][key])
        plt.loglog(vmr, pressure, label=label, ls=ls_elements)

    if xlim is None:
        plt.xlim([1e-12, 1e0])
    else:
        plt.xlim(xlim)

    if ylim is None:
        plt.ylim(p_lim)
    else:
        plt.ylim(ylim)

    plt.xlabel('Volume Mixing Ratio')
    plt.ylabel(f'Pressure ({p_unit})')

    if legend:
        plt.legend()
