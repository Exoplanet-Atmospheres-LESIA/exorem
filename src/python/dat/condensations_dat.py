"""
Functions to calculate condensation profiles.
"""
import numpy as np

from src.python.interface import path_to_results, load_result


def get_condensation_vmr(species, file_suffix, vmr_prefix='vmr',
                         path=path_to_results):
    """
    Get the VMR of a species approximately at its condensation level based on cloud information.
    :param species: species to get the condensation VMR
    :param file_suffix: file suffix
    :param vmr_prefix: vmr files prefix
    :param path: path to the files
    :return: the VMR at the condensation level
    """
    species_vmr = load_result(f'{path}{vmr_prefix}_{file_suffix}')[f'volume_mixing_ratio_{species}']

    try:
        species_cloud_vmr = \
            load_result(f'{path}{vmr_prefix}_{file_suffix}')[f'cloud_opacity_{species}']
    except KeyError:
        # The cloud opacity was not detected, find the latest level the VMR profile was constant
        wh = np.where(species_vmr[1:] - species_vmr[:-1] == 0)[0]

        if np.size(wh) == 0:
            # The profile is never constant, take the VMR at the top of the atmosphere
            wh = 0
        else:
            wh = wh[-1]

        return species_vmr[wh]

    wh = np.where(species_cloud_vmr > 0)

    if np.size(wh) > 0:
        wh = wh[0][-1]

        return species_vmr[wh]
    else:  # cloud has not condensed, return the VMR at the top of the atmosphere
        return species_vmr[0]


def get_h2o_saturation_pressure(temperature):
    """
    Calculate the H2O pressure of saturation.
    Sources:
        - Fray & Schmitt 2009 (Ice-I sublimation)
        - Wagner and Pruss 1993 (evaporation)
        - Wagner and Pruss 1994 (Ice-I, III, V and VI evaporation)
        - Lin et al. 2004 (Ice-VII sublimation)
    :param temperature: (K) temperature
    :return: (bar) H2O saturation pressure
    """
    temperature_triple_point = 273.16  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.11657e-3  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_ice7_triple_point = 355  # (K) temperature of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)
    pressure_ice7_triple_point = 2.17e4  # (bar) pressure of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)

    temperature_critical_point = 647.096  # (K) temperature of H2O critical point (IAPWS 2011)
    pressure_critical_point = 220.64  # (bar) pressure of H2O critical point (IAPWS 2011)

    if temperature <= temperature_triple_point:  # semi-empirical sublimation pressure
        # Coefficients from Feistel and Wagner 2007
        e = [20.996967, 3.724375, -13.920548, 29.698877, -40.197239, 29.788048, -9.130510]
        eta = 0

        for i, ei in enumerate(e):
            eta += ei * (temperature / temperature_triple_point) ** i

        saturation_pressure = pressure_triple_point * \
            np.exp(3 / 2 * np.log(temperature / temperature_triple_point) +
                   (1 - temperature_triple_point / temperature) * eta)
    elif temperature <= temperature_critical_point:  # semi-empirical condensation pressure
        # Coefficients from Wagner and Pruss 1993 (IAPWS 1992)
        # This formula misses the triple point by less than 0.08 percent
        a = [-7.85951783, 1.84408259, -11.7866497, 22.6807411, -15.9618719, 1.80122502]
        tau = 1 - temperature / temperature_critical_point

        saturation_pressure = pressure_critical_point * np.exp(temperature_critical_point / temperature * (
                a[0] * tau +
                a[1] * tau ** 1.5 +
                a[2] * tau ** 3 +
                a[3] * tau ** 3.5 +
                a[4] * tau ** 4 +
                a[5] * tau ** 7.5
        ))
    else:  # species behave like a super-critical fluid
        # Formulae from Lin et al. 2004
        pressure_c = 0.85e4
        alpha = 3.47
        saturation_pressure = \
            pressure_c * ((temperature / temperature_ice7_triple_point) ** alpha) + pressure_ice7_triple_point

    return saturation_pressure


def get_h2o_melting_pressure(temperature):
    """
    Calculate the melting perssure of H2O.
    :param temperature: (K) temperature
    :return: (bar) array containing the melting pressure of ice-1 and ice-3, 5, 6 and 7 at the given temperature
    """
    temperature_triple_point = 273.16  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.11657e-3  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_ice1_ice3_triple_point = 251.165  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice1_ice3_triple_point = 209.9e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice3_ice5_triple_point = 256.164  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice3_ice5_triple_point = 350.1e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice5_ice6_triple_point = 273.31  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice5_ice6_triple_point = 632.4e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice7_triple_point = 355  # (K) temperature of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)
    pressure_ice7_triple_point = 2.17e4  # (bar) pressure of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)

    if temperature_triple_point >= temperature >= temperature_ice1_ice3_triple_point:
        melting_pressure_ice1 = 1 - 0.626000e6 * (1 - (temperature / temperature_triple_point) ** -3) + \
                                0.197135e6 * (1 - (temperature / temperature_triple_point) ** 21.2)
        melting_pressure_ice1 *= pressure_triple_point
    else:
        melting_pressure_ice1 = np.nan

    if temperature < temperature_ice1_ice3_triple_point:
        melting_pressure = np.nan
    elif temperature <= temperature_ice3_ice5_triple_point:
        melting_pressure = 1 - 0.295252 * (1 - (temperature / temperature_ice1_ice3_triple_point) ** 60)
        melting_pressure *= pressure_ice1_ice3_triple_point
    elif temperature <= temperature_ice5_ice6_triple_point:
        melting_pressure = 1 - 1.18721 * (1 - (temperature / temperature_ice3_ice5_triple_point) ** 8)
        melting_pressure *= pressure_ice3_ice5_triple_point
    elif temperature <= temperature_ice7_triple_point:
        # The power coefficient in the paper is 4.6, but this overshoot the l-ice6-ice7 3 point
        melting_pressure = 1 - 1.07476 * (1 - (temperature / temperature_ice5_ice6_triple_point) ** 4.5)
        melting_pressure *= pressure_ice5_ice6_triple_point
    else:  # liquid up to the temperature triple point, then super-critical fluid
        # Formulae from Lin et al. 2004
        pressure_c = 0.85e4
        alpha = 3.47
        melting_pressure = \
            pressure_c * ((temperature / temperature_ice7_triple_point) ** alpha - 1) + pressure_ice7_triple_point
    return melting_pressure_ice1, melting_pressure


def get_nh3_saturation_pressure(temperature):
    """
    Get the NH3 saturation pressure.
    :param temperature: (K) temp
    :return: (bar) saturation pressure of NH3 at the given temperature
    """
    temperature_triple_point = 195.41  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.09e-2  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_critical_point = 405.5  # (K) temperature of H2O critical point (Lide 2006)
    pressure_critical_point = 113.5  # (bar) pressure of H2O critical point (Lide 2006)

    a = np.array([1.596E1, -3.537E3, -3.310E4, 1.742E6, -2.995E7])
    saturation_pressures_ref = np.array([
        pressure_triple_point * 1e2,
        8.7,
        12.6,
        17.9,
        24.9,
        34.1,
        45.9,
        60.8,
        79.6,
        103,
        131,
        165,
        207,
        256,
        313,
        381,
        460,
        552,
        655,
        774,
        909,
        1062,
        pressure_critical_point * 1e2
    ]) * 1e-2
    temperatures_ref = np.append([temperature_triple_point], np.linspace(200, 300, 21))
    temperatures_ref = np.append(temperatures_ref, [temperature_critical_point])
    nh3_saturation_pressure = 0

    if temperature <= temperature_triple_point:
        for i, ai in enumerate(a):
            nh3_saturation_pressure = nh3_saturation_pressure + ai / temperature ** i

        return np.exp(nh3_saturation_pressure)
    elif temperature <= temperature_critical_point:
        return np.exp(np.interp(np.log(temperature), np.log(temperatures_ref), np.log(saturation_pressures_ref)))
    else:
        return 1e10  # arbitrary value


def get_h2o_saturation_temperature(pressures, file_suffix, **kwargs):
    """
    Get the H2O saturation temperatures at given pressures.
    :param pressures: (bar) pressures
    :param file_suffix: VMR file suffix
    :param kwargs: keyword arguments for get_condensation_vmr
    :return: (K) array of H2O condensation temperatures
    """
    condensation_pressure = []
    condensation_temperature_h2o = []
    temperature_grid = np.linspace(1, 3000, 3000)

    for i, t in enumerate(temperature_grid):
        condensation_pressure.append(get_h2o_saturation_pressure(t))

    condensation_pressure = np.asarray(condensation_pressure)
    condensation_vmr = get_condensation_vmr('H2O', file_suffix, **kwargs)

    for i, p in enumerate(pressures):
        condensation_temperature_h2o.append(
            np.interp(p * condensation_vmr, condensation_pressure, temperature_grid)
        )

    return np.asarray(condensation_temperature_h2o)


def get_h2o_melting_temperature(pressures, file_suffix, **kwargs):
    """
    Get the H2O melting temperature.
    :param pressures: (bar) list of pressures
    :param file_suffix: VMR file suffix
    :param kwargs: keyword arguments for get_condensation_vmr
    :return:
    """
    melting_pressure_ice1 = []
    melting_pressure = []
    melting_temperature_ice1 = []
    melting_temperature_h2o = []
    temperature_grid_ice1 = np.linspace(1, 251.164, 2)
    temperature_grid_ice1 = np.append(temperature_grid_ice1, np.linspace(251.165, 273.16, 100))
    temperature_grid_ice1 = np.append(temperature_grid_ice1, np.linspace(273.17, 3000, 2))
    temperature_grid = np.linspace(1, 251.164, 2)
    temperature_grid = np.append(temperature_grid, np.linspace(251.165, 256.164, 50))
    temperature_grid = np.append(temperature_grid, np.linspace(256.164, 273.31, 40))
    temperature_grid = np.append(temperature_grid, np.linspace(273.31, 354, 82))
    temperature_grid = np.append(temperature_grid, np.linspace(355, 3000, 2646))

    for i, t in enumerate(temperature_grid_ice1):
        melting_pressure_ice1.append(get_h2o_melting_pressure(t)[0])

    for i, t in enumerate(temperature_grid):
        melting_pressure.append(get_h2o_melting_pressure(t)[1])

    melting_pressure_ice1 = np.asarray(melting_pressure_ice1)
    melting_pressure = np.asarray(melting_pressure)
    condensation_pressure_vmr = get_condensation_vmr('H2O', file_suffix, **kwargs)

    for i, p in enumerate(pressures):
        melting_temperature_ice1.append(
            np.interp(p * condensation_pressure_vmr, melting_pressure_ice1[::-1], temperature_grid_ice1[::-1])
        )
        melting_temperature_h2o.append(np.interp(p * condensation_pressure_vmr, melting_pressure, temperature_grid))
    return np.asarray(melting_temperature_ice1), np.asarray(melting_temperature_h2o)


def get_nh3_saturation_temperature(pressures, file_suffix, **kwargs):
    """
    Get the NH3 saturation temperatures at given pressures.
    :param pressures: (bar) pressures
    :param file_suffix: VMR file suffix
    :param kwargs: keyword arguments for get_condensation_vmr
    :return: (K) array of NH3 condensation temperatures
    """
    condensation_pressure = []
    condensation_temperature_nh3 = []
    temperature_grid = np.linspace(1, 3000, 3000)

    for i, t in enumerate(temperature_grid):
        condensation_pressure.append(get_nh3_saturation_pressure(t))

    condensation_pressure = np.asarray(condensation_pressure)
    condensation_vmr = get_condensation_vmr('NH3', file_suffix, **kwargs)

    for i, p in enumerate(pressures):
        condensation_temperature_nh3.append(np.interp(p * condensation_vmr, condensation_pressure, temperature_grid))

    return np.asarray(condensation_temperature_nh3)


def get_condensation_temperatures(pressure, species, file_suffix, metallicity, **kwargs):
    """
    Get the condensation temperatures of a given species.
    :param pressure: (bar) pressure
    :param species: species name (e.g. 'H2O')
    :param file_suffix: VMR file suffix
    :param metallicity: atmospheric metallicity
    :param kwargs: keyword arguments for H2O and NH3 get_saturation_temperature
    :return: the condensation temperature of the given species
    """
    if species == 'H2O':
        condensation_temperature = get_h2o_saturation_temperature(
            pressure, file_suffix, **kwargs
        )
    elif species == 'NH3':
        condensation_temperature = get_nh3_saturation_temperature(
            pressure, file_suffix, **kwargs
        )
    else:
        condensation_temperature = {
            'NH4SH': 10 ** 4 / (48.91 - 4.15 * np.log10(pressure) - 4.15 * np.log10(metallicity)),
            'NH4Cl': 10 ** 4 / (27.46 - 2.23 * (np.log10(pressure) + 0.33 * np.log10(metallicity))),
            # NH4Cl source: https://pubchem.ncbi.nlm.nih.gov/source/hsdb/483#section=Taste
            'ZnS': 10 ** 4 / (12.527 - 0.63 * np.log10(pressure) - 1.26 * np.log10(metallicity)),
            'KCl': 10 ** 4 / (12.479 - 0.879 * np.log10(pressure) - 0.879 * np.log10(metallicity)),
            'Na2S': 10 ** 4 / (10.045 - 0.72 * np.log10(pressure) - 0.5 * np.log10(metallicity)),
            'MnS': 10 ** 4 / (7.447 - 0.42 * np.log10(pressure) - 0.84 * np.log10(metallicity)),
            'Cr': 10 ** 4 / (6.576 - 0.486 * np.log10(pressure) - 0.486 * np.log10(metallicity)),
            'MgSiO3': 10 ** 4 / (6.26 - 0.35 * np.log10(pressure) - 0.7 * np.log10(metallicity)),
            'Mg2SiO4': 10 ** 4 / (5.89 - 0.37 * np.log10(pressure) - 0.73 * np.log10(metallicity)),
            'Fe': 10 ** 4 / (5.44 - 0.48 * np.log10(pressure) - 0.48 * np.log10(metallicity)),
            'Al2O3': 10 ** 4 / (5.014 - 0.2179 * np.log10(pressure) - 0.58 * np.log10(metallicity)),
        }

        condensation_temperature = condensation_temperature[species]

    return condensation_temperature


def get_h2o_triple_point(pressure, condensation_temperature_h2o):
    """
    Return the partial pressure of the H2O ice1-liquid-gas triple point.
    :param pressure: (bar) list of pressures
    :param condensation_temperature_h2o: (K) list of H2O condensation temperatures
    :return: (bar) the partial pressure of the H2O ice1-liquid-gas triple point
    """
    for i, t in enumerate(condensation_temperature_h2o):
        if t > 273.16:
            pressure_h2o_triple_point = np.interp(
                273.16, (condensation_temperature_h2o[i - 1], condensation_temperature_h2o[i]),
                (pressure[i - 1], pressure[i])
            )

            return pressure_h2o_triple_point

    return np.max(pressure)


def get_h2o_condensation_pressure(pressure, temperature, condensation_temperature_h2o):
    """
    Return the pressure of condensation of H2O
    :param pressure: (bar) array of pressures
    :param temperature: (K) array of temperatures
    :param condensation_temperature_h2o: (K) H2O condensation temperatures at the corresponding pressures
    :return: (bar) the pressure of condensation of H2O
    """
    wh = np.where(condensation_temperature_h2o > temperature)

    if np.size(wh) > 0:
        return pressure[wh[0][-1]]
    else:
        return np.nan
