"""
Useful constants. Uses scipy constants for most common physical constants (based on the most recent COdata report).
For other constants, mostly astronomical, source is indicated between parenthesis.
Sources:
    - IAU 2015: https://arxiv.org/pdf/1510.07674.pdf
"""
from scipy.constants import *


m_hydrogen = 1.00794 * atomic_mass

GM_sol = 1.3271244e20  # (m3.s-2) solar gravitational constant (IAU 2015)
GM_earth = 3.986004e14   # (m3.s-2) earth gravitational constant (IAU 2015)
GM_jup = 1.2668653e17  # (m3.s-2) jupiter gravitational constant (IAU 2015)

M_sol = GM_sol / G  # (kg) mass of the Sun
R_sol = 6.957E8  # (m) radius of the Sun (IAU 2015)
L_sol = 3.828E26  # (W) luminosity of the Sun (IAU 2015)
T_eff_sol = 5772  # (K) effective temperature of the Sun (IAU 2015)
rho_sol = 1408.0  # (kg.m-3) density of the Sun
Z_sol = -4.33  # (log(N_Fe / N_H)) metallicity of the Sun

M_chandrasekhar = 2.018236 / 2 * (3 * pi) ** 0.5 * \
                  (h / (2 * pi) * c / G) ** 1.5 / (2 * m_hydrogen) ** 2  # (kg) Chandrasekhar mass limit

M_jup = GM_jup / G  # (kg) mass of Jupiter
R_jup = 7.1492e7  # (m) (equatorial) radius of Jupiter (IAU 2015)

M_earth = GM_earth / G  # (kg) mass of Earth
R_earth = 6.3781e6  # (m) (equatorial) radius of Earth (IAU 2015)

g0 = 9.80665  # (m.s-2) standard gravity

AU = astronomical_unit  # (m) astronomical unit
ly = light_year  # (m) light year
pc = parsec  # (m) parsec

lifetime_sol = 10 ** 10 * year  # (s) lifetime of the Sun
age_universe = 13 * 10 ** 9 * year  # (s) age of the universe


# Units conversion functions
def sphere_solid_angle_view(radius, distance):
    """
    Get the solid angle view of sphere from a given distance for small viewing angles (cos(a) ~ 1 - a**2/2).
    :param radius: (m) radius of the sphere
    :param distance: (m) distance from the center of the sphere
    :return: the solid angle view of the sphere
    """
    return pi * (radius / distance) ** 2


def radiosity_cm2radiosity_um(radiosity_cm, wavenumber):
    """
    Convert a radiosity from W.m-2/cm-1 to W.m-2/um.
    :param radiosity_cm: (W.m-2/cm-1) radiosity
    :param wavenumber: (cm-1) wavenumber corresponding to the radiosity
    :return: (W.m-2/um) converted radiosity
    """
    return radiosity_cm * 1e-4 * wavenumber ** 2


def wavenumber2wavelength(wavenumber):
    """
    Convert a wavenumber in cm-1 to a wavelength in m.
    :param wavenumber: (cm-1) wavenumber
    :return: (m) the corresponding wavelength
    """
    return 1e-2 / wavenumber


# Elements list and symbols (Wikipedia, 20/01/2019)
symbol_name_list = """\
H	Hydrogen	
He	Helium	    
Li	Lithium	    
Be	Beryllium	
B	Boron	    
C	Carbon	    
N	Nitrogen	
O	Oxygen	    
F	Fluorine	
Ne	Neon	    
Na	Sodium	    
Mg	Magnesium	
Al	Aluminium	
Si	Silicon	    
P	Phosphorus	
S	Sulfur	    
Cl	Chlorine	
Ar	Argon	    
K	Potassium	
Ca	Calcium	    
Sc	Scandium	
Ti	Titanium	
V	Vanadium	
Cr	Chromium	
Mn	Manganese	
Fe	Iron	    
Co	Cobalt	    
Ni	Nickel	    
Cu	Copper	    
Zn	Zinc	    
Ga	Gallium	    
Ge	Germanium	
As	Arsenic	    
Se	Selenium	
Br	Bromine	    
Kr	Krypton	    
Rb	Rubidium	
Sr	Strontium	
Y	Yttrium	    
Zr	Zirconium	
Nb	Niobium	    
Mo	Molybdenum	
Tc	Technetium	
Ru	Ruthenium	
Rh	Rhodium	    
Pd	Palladium	
Ag	Silver	    
Cd	Cadmium	    
In	Indium	    
Sn	Tin	        
Sb	Antimony	
Te	Tellurium	
I	Iodine	    
Xe	Xenon	    
Cs	Caesium	    
Ba	Barium	    
La	Lanthanum	
Ce	Cerium	    
Pr	Praseodymium
Nd	Neodymium	
Pm	Promethium	
Sm	Samarium	
Eu	Europium	
Gd	Gadolinium	
Tb	Terbium	    
Dy	Dysprosium	
Ho	Holmium	    
Er	Erbium	    
Tm	Thulium	    
Yb	Ytterbium	
Lu	Lutetium	
Hf	Hafnium	    
Ta	Tantalum	
W	Tungsten	
Re	Rhenium	    
Os	Osmium	    
Ir	Iridium	    
Pt	Platinum	
Au	Gold	    
Hg	Mercury	    
Tl	Thallium    
Pb	Lead	    
Bi	Bismuth	    
Po	Polonium	
At	Astatine	
Rn	Radon	    
Fr	Francium	
Ra	Radium	    
Ac	Actinium	
Th	Thorium	    
Pa	Protactinium
U	Uranium	    
Np	Neptunium	
Pu	Plutonium	
Am	Americium	
Cm	Curium	    
Bk	Berkelium	
Cf	Californium	
Es	Einsteinium	
Fm	Fermium 	
Md	Mendelevium	
No	Nobelium	
Lr	Lawrencium	
Rf	Rutherfordium
Db	Dubnium	    
Sg	Seaborgium	
Bh	Bohrium	    
Hs	Hassium	    
Mt	Meitnerium	
Ds	Darmstadtium
Rg	Roentgenium	
Cn	Copernicium	
Nh	Nihonium	
Fl	Flerovium	
Mc	Moscovium	
Lv	Livermorium	
Ts	Tennessine	
Og	Oganesson"""


def parse_elements_name_and_symbol(string):
    # Initialise names and symbols with "element zero" aka "neutronium"
    names = ['Neutronium']
    symbols = ['n']

    for line in string.split('\n'):
        columns = line.split()
        symbols.append(columns[0])
        names.append(columns[1])

    return names, symbols


elements_name, elements_symbol = parse_elements_name_and_symbol(symbol_name_list)
