module h5_interface
    use hdf5

    implicit none

    character(len=*), parameter :: &
        hdf5_files_extension = '.h5'  ! output files extension

    contains
        subroutine load_h5_dataset_1d_dp(file, dataset_name, dataset, loc_id)
            ! """
            ! Load a HDF5 1-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :return dataset: the dataset
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            doubleprecision, dimension(:), allocatable, intent(out) :: &
                dataset

            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, local_id
            integer(HSIZE_T), dimension(:), allocatable :: data_shape, data_shape_max

            integer :: n_dims

            ! Open file and dataset
            call h5fopen_f(file, H5F_ACC_RDONLY_F, file_id, error)

            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            call h5dopen_f(local_id, dataset_name, dataset_id, error)

            ! Get dataset shape
            call h5dget_space_f(dataset_id, dataspace_id, error)
            call h5sget_simple_extent_ndims_f(dataspace_id, n_dims, error)

            if(n_dims /= rank(dataset)) then
                write(*, '("Error: loaded dataset ''", A, "'' must be of rank ", I0, " but is of rank ", I0)') &
                    trim(dataset_name), rank(dataset), n_dims
                stop
            end if

            allocate(data_shape(n_dims), data_shape_max(n_dims))
            call h5sget_simple_extent_dims_f(dataspace_id, data_shape, data_shape_max, error)

            ! Allocate dataset
            allocate(dataset(data_shape(1)))

            ! Read dataset
            call h5dread_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, data_shape, error)

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)
        end subroutine load_h5_dataset_1d_dp


        subroutine load_h5_dataset_2d_dp(file, dataset_name, dataset)
            ! """
            ! Load a HDF5 2-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :return dataset: the dataset
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name

            doubleprecision, dimension(:, :), allocatable, intent(out) :: &
                dataset

            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id
            integer(HSIZE_T), dimension(:), allocatable :: data_shape, data_shape_max

            integer :: n_dims

            ! Open file and dataset
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)
            call h5dopen_f(file_id, dataset_name, dataset_id, error)

            ! Get dataset shape
            call h5dget_space_f(dataset_id, dataspace_id, error)
            call h5sget_simple_extent_ndims_f(dataspace_id, n_dims, error)

            if(n_dims /= rank(dataset)) then
                write(*, '("Error: loaded dataset ''", A, "'' must be of rank ", I0, " but is of rank ", I0)') &
                    trim(dataset_name), rank(dataset), n_dims
                stop
            end if

            allocate(data_shape(n_dims), data_shape_max(n_dims))
            call h5sget_simple_extent_dims_f(dataspace_id, data_shape, data_shape_max, error)

            ! Allocate dataset
            allocate(dataset(data_shape(1), data_shape(2)))

            ! Read dataset
            call h5dread_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, data_shape, error)

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)
        end subroutine load_h5_dataset_2d_dp


        subroutine load_h5_dataset_4d_dp(file, dataset_name, dataset)
            ! """
            ! Load a HDF5 4-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :return dataset: the dataset
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name

            doubleprecision, dimension(:, :, :, :), allocatable, intent(out) :: &
                dataset

            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id
            integer(HSIZE_T), dimension(:), allocatable :: data_shape, data_shape_max

            integer :: n_dims

            ! Open file and dataset
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)
            call h5dopen_f(file_id, dataset_name, dataset_id, error)

            ! Get dataset shape
            call h5dget_space_f(dataset_id, dataspace_id, error)
            call h5sget_simple_extent_ndims_f(dataspace_id, n_dims, error)

            if(n_dims /= rank(dataset)) then
                write(*, '("Error: loaded dataset ''", A, "'' must be of rank ", I0, " but is of rank ", I0)') &
                    trim(dataset_name), rank(dataset), n_dims
                stop
            end if

            allocate(data_shape(n_dims), data_shape_max(n_dims))
            call h5sget_simple_extent_dims_f(dataspace_id, data_shape, data_shape_max, error)

            ! Allocate dataset
            allocate(dataset(data_shape(1), data_shape(2), data_shape(3), data_shape(4)))

            ! Read dataset
            call h5dread_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, data_shape, error)

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)
        end subroutine load_h5_dataset_4d_dp


        subroutine load_h5_string(file, dataset_name, string)
            ! """
            ! Load a HDF5 1-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :return dataset: the dataset
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name

            integer, parameter :: dim0 = 1
            integer, parameter :: sdim = 255
            integer(HID_T) :: file_id, dataset_id, dataspace_id, file_type
            integer :: error
            integer(HSIZE_T), dimension(1:1) :: data_shape
            integer(HSIZE_T), dimension(1:2) :: data_shape_max

            integer(SIZE_T), dimension(1) :: str_len = [255]
            integer(HSIZE_T), dimension(2) :: data_dims  = [sdim, dim0]
            character(len=sdim), dimension(:), allocatable :: dataset

            character(len=sdim), intent(out) :: &
                string

            ! Open file and dataset
            call h5fopen_f(file, H5F_ACC_RDONLY_F, file_id, error)
            call h5dopen_f(file_id, dataset_name, dataset_id, error)

            call h5dget_type_f(dataset_id, file_type, error)

            ! Get dataset shape
            call h5dget_space_f(dataset_id, dataspace_id, error)
            call h5sget_simple_extent_dims_f(dataspace_id, data_shape, data_shape_max, error)

            ! Allocate dataset
            allocate(dataset(1:data_shape(1)))

            ! Read dataset
            call h5dread_vl_f(dataset_id, file_type, dataset, data_dims, str_len, error, dataspace_id)

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)

            string = dataset(1)
        end subroutine load_h5_string


        subroutine write_h5_dataset_0d_dp(file, dataset_name, dataset, attribute_name, attribute, &
            loc_id, sub_group_name)
            ! """
            ! Write a HDF5 scalar double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :param sub_group_name: name of the dataset's sub group (dataset is in file/.../group[loc_id]/subgroup)
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            character(len=*), optional, intent(in) :: &
                sub_group_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            doubleprecision, intent(in) :: &
                dataset

            logical :: group_exists
            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id, sub_group_id, local_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Check if group exists, create it if needed
            if(present(sub_group_name)) then
                call h5lexists_f(local_id, sub_group_name, group_exists, error)

                if(group_exists) then
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                else
                    call h5gcreate_f(local_id, sub_group_name, sub_group_id, error)
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                end if

                local_id = sub_group_id
            end if

            call h5dcreate_f(local_id, dataset_name, H5T_NATIVE_DOUBLE, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(local_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)

            if(present(sub_group_name)) then
                call h5gclose_f(sub_group_id, error)
            end if

            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_0d_dp


        subroutine write_h5_dataset_1d_dp(file, dataset_name, dataset, attribute_name, attribute, &
            loc_id, sub_group_name)
            ! """
            ! Write a HDF5 1-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :param sub_group_name: name of the dataset's sub group (dataset is in file/.../group[loc_id]/subgroup)
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            character(len=*), optional, intent(in) :: &
                sub_group_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            doubleprecision, dimension(:), intent(in) :: &
                dataset

            logical :: group_exists
            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id, sub_group_id, local_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Check if group exists, create it if needed
            if(present(sub_group_name)) then
                call h5lexists_f(local_id, sub_group_name, group_exists, error)

                if(group_exists) then
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                else
                    call h5gcreate_f(local_id, sub_group_name, sub_group_id, error)
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                end if

                local_id = sub_group_id
            end if

            call h5dcreate_f(local_id, dataset_name, H5T_NATIVE_DOUBLE, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(local_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)

            if(present(sub_group_name)) then
                call h5gclose_f(sub_group_id, error)
            end if

            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_1d_dp


        subroutine write_h5_dataset_2d_dp(file, dataset_name, dataset, attribute_name, attribute, &
            loc_id, sub_group_name)
            ! """
            ! Write a HDF5 2-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :param sub_group_name: name of the dataset's sub group (dataset is in file/.../group[loc_id]/subgroup)
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            character(len=*), optional, intent(in) :: &
                sub_group_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            doubleprecision, dimension(:, :), intent(in) :: &
                dataset

            logical :: group_exists
            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id, sub_group_id, local_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Check if group exists, create it if needed
            if(present(sub_group_name)) then
                call h5lexists_f(local_id, sub_group_name, group_exists, error)

                if(group_exists) then
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                else
                    call h5gcreate_f(local_id, sub_group_name, sub_group_id, error)
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                end if

                local_id = sub_group_id
            end if

            call h5dcreate_f(local_id, dataset_name, H5T_NATIVE_DOUBLE, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(local_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)

            if(present(sub_group_name)) then
                call h5gclose_f(sub_group_id, error)
            end if

            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_2d_dp


        subroutine write_h5_dataset_4d_dp(file, dataset_name, dataset, attribute_name, attribute)
            ! """
            ! Write a HDF5 4-dimensions array double precision dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            doubleprecision, dimension(:, :, :, :), intent(in) :: &
                dataset

            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)
            call h5dcreate_f(file_id, dataset_name, H5T_NATIVE_DOUBLE, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(file_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_DOUBLE, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_4d_dp


        subroutine write_h5_dataset_0d_int(file, dataset_name, dataset, attribute_name, attribute, &
            loc_id, sub_group_name)
            ! """
            ! Write a HDF5 scalar integer dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :param sub_group_name: name of the dataset's sub group (dataset is in file/.../group[loc_id]/subgroup)
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            character(len=*), optional, intent(in) :: &
                sub_group_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            integer, intent(in) :: &
                dataset

            logical :: group_exists
            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id, sub_group_id, local_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Check if group exists, create it if needed
            if(present(sub_group_name)) then
                call h5lexists_f(local_id, sub_group_name, group_exists, error)

                if(group_exists) then
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                else
                    call h5gcreate_f(local_id, sub_group_name, sub_group_id, error)
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                end if

                local_id = sub_group_id
            end if

            call h5dcreate_f(local_id, dataset_name, H5T_NATIVE_INTEGER, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(local_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_INTEGER, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)

            if(present(sub_group_name)) then
                call h5gclose_f(sub_group_id, error)
            end if

            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_0d_int


        subroutine write_h5_dataset_1d_int(file, dataset_name, dataset, attribute_name, attribute, &
            loc_id, sub_group_name)
            ! """
            ! Write a HDF5 1-dimensions array integer dataset.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param dataset_name: name of the dataset
            ! :param dataset: the dataset to write
            ! :param attribute_name: name of the scalar attribute of the dataset
            ! :param attribute: scalar attribute of the dataset
            ! :param loc_id: HDF5 ID of the dataset's group
            ! :param sub_group_name: name of the dataset's sub group (dataset is in file/.../group[loc_id]/subgroup)
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                dataset_name, &
                attribute_name, &
                attribute

            character(len=*), optional, intent(in) :: &
                sub_group_name

            integer(HID_T), optional, intent(in) :: &
                loc_id

            integer, dimension(:), intent(in) :: &
                dataset

            logical :: group_exists
            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, attr_id, aspace_id, atype_id, sub_group_id, local_id
            integer(SIZE_T) :: attrlen
            integer(HSIZE_T), dimension(rank(dataset)) :: dims
            integer :: rank_data

            rank_data = rank(dataset)
            dims = shape(dataset)

            attrlen = len(trim(attribute))

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Create the dataspace of the dataset, then the dataset itself
            call h5screate_simple_f(rank_data, dims, dataspace_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Check if group exists, create it if needed
            if(present(sub_group_name)) then
                call h5lexists_f(local_id, sub_group_name, group_exists, error)

                if(group_exists) then
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                else
                    call h5gcreate_f(local_id, sub_group_name, sub_group_id, error)
                    call h5gopen_f(local_id, sub_group_name, sub_group_id, error)
                end if

                local_id = sub_group_id
            end if

            call h5dcreate_f(local_id, dataset_name, H5T_NATIVE_INTEGER, dataspace_id, dataset_id, error)

            ! Open and save the dataset
            call h5dopen_f(local_id, dataset_name, dataset_id, error)
            call h5dwrite_f(dataset_id, H5T_NATIVE_INTEGER, dataset, dims, error)

            ! Add the attribute
            if(attribute_name /= '') then
                call h5screate_f(H5S_SCALAR_F, aspace_id, error)
                call h5tcopy_f(H5T_NATIVE_CHARACTER, atype_id, error)
                call h5tset_size_f(atype_id, attrlen, error)

                call h5acreate_f(dataset_id, attribute_name, atype_id, aspace_id, attr_id, error)
                call h5awrite_f(attr_id, atype_id, attribute, dims, error)
                call h5aclose_f(attr_id, error)
                call h5sclose_f(aspace_id, error)
            end if

            ! Close dataset and file
            call h5dclose_f(dataset_id, error)

            if(present(sub_group_name)) then
                call h5gclose_f(sub_group_id, error)
            end if

            call h5fclose_f(file_id, error)
        end subroutine write_h5_dataset_1d_int


        subroutine write_h5_string(file, string_name, string, loc_id)
            ! """
            ! Write a HDF5 string.
            ! HDF5 fortran interface must be initialized.
            ! :param file: HDF5 file
            ! :param string_name: name of the string
            ! :param string: the string to write
            ! :param attribute_name: name of the scalar attribute of the string
            ! :param attribute: scalar attribute of the string
            ! """
            implicit none

            character(len=*), intent(in) :: &
                file, &
                string_name, &
                string

            integer(HID_T), optional, intent(in) :: &
                loc_id

            integer :: error
            integer(HID_T) :: file_id, dataset_id, dataspace_id, datatype_id, local_id
            integer(SIZE_T) :: string_length
            integer(HSIZE_T), dimension(1) :: dims

            string_length = len(trim(string))
            dims(1) = string_length

            ! Open file
            call h5fopen_f(file, H5F_ACC_RDWR_F, file_id, error)

            ! Write the dataset in group if needed
            if(present(loc_id)) then
                local_id = loc_id
            else
                local_id = file_id
            end if

            ! Create the dataspace of the string, then the string itself
            call h5screate_f(H5S_SCALAR_F, dataspace_id, error)
            call h5tcopy_f(H5T_NATIVE_CHARACTER, datatype_id, error)
            call h5tset_size_f(datatype_id, string_length, error)

            call h5dcreate_f(local_id, string_name, datatype_id, dataspace_id, dataset_id, error)

            ! Open and save the string
            call h5dopen_f(local_id, string_name, dataset_id, error)
            call h5dwrite_f(dataset_id, datatype_id, string, dims, error)

            ! Close string and file
            call h5dclose_f(dataset_id, error)
            call h5fclose_f(file_id, error)
        end subroutine write_h5_string
end module h5_interface