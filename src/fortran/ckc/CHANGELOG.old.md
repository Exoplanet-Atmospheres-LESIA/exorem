# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com)
and this project adheres to [Semantic Versioning](http://semver.org).

Versioning before version 3.0.0 is a rework of the pseudo-changelog found inside the software _main_ file. Version are
numbered afterward based on the information found in this pseudo-changelog.
Legacy code (before version 3.0.0) by Richard F. Thompson and Richard Achterberg.

## [6.0.0] - 2019-08-29
From this version, this project will be totally integrated inside the Pytheas project, hence this file will no longer
be updated. All notable changes to this will be documented in the Pytheas Changelog file.

### Changed
- Complete integration in Pyhteas project.
- File `main.f90` renamed `ckc.f90`, and program `create_k_ditrib_lattice` renamed `ckc`.
- Distributions files no longer outputted, they are now concatenated in a single file.
- Input parameters file no longer needs to be explicitly mentioned using `<` when launching the code.
- Input parameters file is now the same as *CAXS*' one.
- Absorption cross sections are now read automatically, even if multiple processes were used to generate them.
- Order of input parameters of subroutine `gauleg`.
- More code cleanup.

### Fixed
- Distribution function not calculated properly when absorption cross sections are not in increasing numerical order. 

## [5.0.0] - 2019-08-23
### Added
- Module `k_correlation`, to store all the functions useful to *k_correlated*.
- Module `thermodynamics`.

### Changed
- Moved subroutines `compute_distribution_function`, `gauleg`, `instrument_function_vector` and `interpolate_k_vec`
inside new module `k_correlation`.
- Moved subroutines `create_distribution_file_name` and `select_nearest_wavenumber` inside the `main` source file.
- Subroutine `ascending_index` replaced by the more efficient subroutine `quicksort_index`.
- More inputs parameters are in common with program *CAXS*.
- Input parameter `gtype` is now a string describing the desired interpolation type rather than an integer.
- Output destribution files are now *.dat* and have an "_" instead of a ".".
- More code cleanup.

### Removed
- Unused "Save k_vec" pseudo-function.

## [4.2.0] - 2019-08-22
### Added
- Module `species`.

### Changed
- Main code arrays are now dynamically allocated.
- More code cleanup.

## [4.1.0] - 2019-08-21
### Added
- Modules `interface` and `math`.
- Subroutine `read_absorption_cross_sections_file`.

### Changed
- More code cleanup.


## [4.0.2] - 2019-08-19
### Changed
- More code cleanup.

### Fixed
- Code trying to read non-existent absorption files.
- Values of type `real(4)` instead of `real(8)` passed to subroutine `gauleg`, generating weird results.

## [4.0.1] - 2019-08-19
### Changed
- Code (mostly) cleaned up.

## [4.0.0] - 2019-08-14
### Changed
- Code is now in Fortran2018 standard (some obsolecences are still here).

## [3.0.0] - 1999-01-05
### Changed
- Modified to use absorption files from Bruno Bezard, which are easier to deal with than the Maguire files, as they have 
only one file for all wavenumbers at each temperature/pressure point. 
- The temperature and pressure and resolution information is now in the files with the absorption data, so we no 
longer need a separate input lattice file.
- The output data allows for a different set of temperatures at each pressure level.

## [2.0.0] - 1997-03-??
### Changed
- Ported from VMS to HP-UX by Richard Achterberg.
- Changed to read program inputs from a namelist, instead of having everything hardcoded in data statements in the 
source code, and to allow arbitrary numbers of g-values in the output.
- Used for methane absorption, not CO2 (actually, nothing in the program is absorber specific).

## [1.3.1] - 1996-11-20
### Fixed
- Ad hoc fix to preclude abortion when variables `num_k` and `NUM_phi` differ from one point, due to a subtle fault in
the spectral alignment of the discrete k-data extracted from one or more files with computed samples of the Instrument 
Function.

## [1.3.0] - 1996-09-20
### Added
- Search a second mag-optical disk for some or all of the Absorption Coefficient Files which contain the k-data needed 
to complete computation of a k-Distribution. (Mag_Op_Path_2)

## [1.2.0] - 1996-08-22
### Added
- A wavenumber loop so that the program can manufacture more than one Distribution Lattice File during one execution of
the program.

## [1.2.0] - 1996-08-21
### Changed
- Distribution file name now contains an encryption of the central wavenumber of all the distributions in the file.

## [1.1.0] - 1996-08-16
### Changed
- Whenever the program is unable to open an Absorption Coefficient file containing data which is needed to compute a
Distribution at the triple (nu,P,T), the (P,T) site in the output file, DISTRIBUTION_nu.DAT, will be filled with minus 
ones.

## [1.0.1] - 1996-08-08
### Changed
- Subroutine `Compute_Full_Distribution` replaced by `Compute_Full_Distribution`.

### Fixed
- A slight error in (g,kg) when ten or twenty equal k-values occur in ten or twenty thousand k-data.

## [1.0.0] - 1996-06-07
### Changed
- Cloned from Create_k_Distr_Latt_File.
### Removed
- Debug code and intermediate data export code that was used during that program's development.

## [0.8.0] - 1994-07-12
### Changed
- Cloned from Create_k_Distribution_Lattice_File.

## [0.7.0] - 1994-01-11
### Changed
- Cloned from Create_k_Distribution_File.

## [0.6.0] - 1993-11-??
### Changed
- Modified to access monochromatic k-data from files prepared by Dr. Gordon Bjoraker who used the so called 
"French Radiation Program".

## [0.5.0] -  1993-09-28
### Changed
- Cloned from Get_Monochromatic_k.
- Values of the Instrument Function are now computed consistent with the wave numbers corresponding to the selected 
coefficient spectrum.

## [0.4.0] - 1993-08-13
### Changed
- Cloned from SSP_Get_Transmittance.

## [0.3.0] - 1993-06-29
### Changed
- The code now prescribes one g-vector for all spectral-k (all levels).

## [0.2.0] - 1993-03-09
### Changed
- The code computed the k-distribution according to the second conjecture, and it prescribed equi-log k-intervals which
caused different g-vectors to be computed for different spectral-k (different levels).

## [0.1.0] - 1993-02-16
### Changed
- Cloned from SSP_Take_a_Look.
- The code computed the k-distribution according to the first conjecture. It confected absorption coefficients, k, from 
monochromatic Transmittance values, T, found in an old SSP Data Base.