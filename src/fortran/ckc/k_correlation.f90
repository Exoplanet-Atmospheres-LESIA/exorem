module k_correlation
    ! """
    ! Contains all the variables and subroutines needed to calculate the k-coefficients.
    ! """
    use math, only: pi
    use thermodynamics, only: n_levels, size_thermospace

    implicit none

    integer, parameter :: &
        size_integration_type = 32

    character(len=size_integration_type) :: &
        gtype ! type of integration used (Gauss-Legendre|Gauss-Legendre2|Gauss-Chebyshev)

    integer :: &
        num_nu_0, &         ! number of wavenumebrs
        num_vec             ! number of k coefficients

    integer, dimension(:), allocatable :: &
        num_t               ! Lattice (P,T) dimensions

    double precision :: &
        resolution          ! (cm-1) width of phi between inflection points

    doubleprecision, dimension(:), allocatable :: &
        nu_0_list, &
        g_vec, &            ! g-Vector Used By Distribution Data Base
        w_vec               ! Quadrature Weights for g_vec

    doubleprecision, dimension(:, :), allocatable :: &
        t_latt              !  Lattice Coordinates

    doubleprecision, dimension(:, :, :, :), allocatable :: &
        k_vec, &            ! k-Distribution Extracted for Data Base

    save

    contains
        subroutine allocate_k_correlation_parameters()
            ! """
            ! Allocate and initialise the k-correlation parameters.
            ! """
            implicit none

            allocate(g_vec(num_vec))
            allocate(k_vec(num_nu_0, n_levels, size_thermospace, num_vec))
            allocate(w_vec(num_vec))
            allocate(num_t(n_levels))
            allocate(t_latt(size_thermospace, n_levels))

            g_vec(:) = 0d0
            k_vec(:, :, :, :) = 0d0
            w_vec(:) = 0d0
            num_t(:) = 0
            t_latt(:, :) = 0d0
        end subroutine allocate_k_correlation_parameters


        subroutine compute_distribution_function(k, num_k, indx, phi_dnu, g, kg, num_g)
            ! """
            ! Compute the distribution function.
            ! This program was cloned from Compute_Full_Distribution, which treats k-data having the same value as
            ! though they are unequal, thereby incuring small error in the inverse function kg when four or five
            ! k-values are equal in an array k containing several thousand k-values. By not recognizing equality in the
            ! k-data, the number of g-values returned by the program is equal to the number of k values, and output
            ! variable num_g is not needed. This subroutine computes the correct measure
            ! when the k-array contains some equal values of k. The inverse function kg is therefore more accurate, but
            ! the number of g will be less than the number of k, and the output variable num_g will have to be used in
            ! subsequent processing.
            ! :param k: spectral k (absorption coefficient)
            ! :param num_k: number of spectral k values
            ! :param indx: ascending Index, k(indx(i)) <= k(indx(i+1))
            ! :param phi_dnu: (phi=instrument function)*(dnu=dWavenumber) phi_dnu = Lebesgue Measure of discrete k-data
            ! :return g: Full Cumulative Frequency Distribution
            ! :return kg: Iiverse of g(k)
            ! :return num_g: the number of values stored in g and kg.
            !"""
            implicit none

            integer, intent(in) :: num_k, indx(num_k)
            doubleprecision, intent(in) :: k(num_k), phi_dnu(num_k)

            integer, intent(out) :: num_g
            doubleprecision, intent(out) :: g(num_k), kg(num_k)

            integer :: &
                i, &        ! index
                j           ! index

            doubleprecision :: &
                k_prev, &   ! previous k to be compared with current k
                sum         ! sum of phi_dnu

            k_prev = -huge(0d0)  !  initialize with an impossible value of k
            num_g = 0
            sum = 0d0

            g(:) = 0d0
            kg(:) = 0d0

            do i = 1, num_k
                j = indx(i)
                sum = sum + phi_dnu(j)

                if(k_prev < k(i) * (1 - 1d-12) .or. k_prev > k(i) * (1 + 1d-12)) then
                    num_g = num_g + 1
                    kg(num_g) = k(i)
                end if

                g(num_g) = sum ! g is the k-ordered accumulation of measure

                k_prev = k(i)
            end do
        end subroutine compute_distribution_function


        subroutine calculate_gauss_legendre_quadrature(x1, x2, n, x, w)
            ! """
            ! Calculate the Gauss-Legendre quadrature between x1 and x2.
            ! :param x1: lower bound of the quadrature
            ! :param x2: higher bound of the quadrature
            ! :param n: number of points where to calculate the quadrature
            ! :return x: quadrature
            ! :return w: weight
            ! """
            implicit none

            integer, intent(in) :: n
            doubleprecision, intent(in) :: x1, x2

            doubleprecision, intent(out) :: x(n), w(n)

            doubleprecision, parameter :: &
                eps = 3d-14

            integer :: &
                i, &
                j, &
                m

            doubleprecision :: &
                p1, &
                p2, &
                p3, &
                pp, &
                xl, &
                xm, &
                z, &
                z1

            m = (n + 1) / 2
            xm = (x2 + x1) / 2d0
            xl = (x2 - x1) / 2d0

            do i = 1, m
                z = cos(pi * (i - 0.25d0) / (n + 0.5d0))
                z1 = huge(0d0)
                pp = huge(0d0)

                do while(abs(z - z1) > eps)
                    p1 = 1d0
                    p2 = 0d0

                    do j = 1, n
                        p3 = p2
                        p2 = p1
                        p1 = (dble(2 * j - 1) * z * p2 - dble(j - 1) * p3) / dble(j)
                    end do

                    pp = n * (z * p1 - p2) / (z ** 2 - 1d0)
                    z1 = z
                    z = z1 - p1 / pp
                end do

                x(i) = xm - xl * z
                x(n + 1 - i) = xm + xl * z
                w(i) = 2d0 * xl / ((1d0 - z ** 2) * pp ** 2)
                w(n + 1 - i) = w(i)
            end do
        end subroutine calculate_gauss_legendre_quadrature


        subroutine interpolate_k_vec(g, kg, num_k, g_vec, num_vec, k_vec)
            ! """
            ! Given the normalized g-vector which is common to the (g,k) pairs at all levels in a k-Distribution
            ! Data Base, this program linearly interpolates a given full k-Distribution and extracts the k-vector
            ! corresponding to the common g-vector. The k-vector can be added to the Data Base for a designated level.
            ! :param g: full abscissae for kg (NOT normalized)
            ! :param kg: k-distribution, kg(g) = g(k)^inverse
            ! :param num_k: number of points in k-Distribution
            ! :param g_vec: normalized g-vector used in Data Base
            ! :param num_vec: number of points in g_vec
            ! :return k_vec: extracted k-vector corresponding to g_vec for a designated level.
            ! """
            implicit none

            integer, intent(in) :: num_k, num_vec
            doubleprecision, intent(in) :: g(*), kg(*), g_vec(*)

            doubleprecision, intent(out) :: k_vec(*)

            integer :: &
                m, &
                n, &
                n1

            doubleprecision :: &
                eps, &
                g_lo, &
                g_val, &
                g_hi, &
                k_lo, &
                k_val, &
                k_hi, &
                del_g, &
                g_

            if(num_k == 1) then
                do m = 1, num_vec
                    k_vec(m) = kg(1)
                end do
            else
                del_g = g(num_k)

                n1 = 1
                eps = 1d-5
                g_lo = 0d0
                k_lo = kg(1)

                do m = 1, num_vec
                    g_val = g_vec(m)  ! normalized

                    do n = n1, num_k
                        g_ = g(n) / del_g  ! normalized g(n)

                        if(abs(g_val - g_) < eps) then
                            k_vec(m) = kg(n)  ! close enough - don't interpolate

                            exit
                        end if

                        if(g_val > g_) then
                            g_lo = g_
                            k_lo = kg(n)

                            cycle
                        end if

                        if(g_val < g_) then  ! interpolate kg to get k_vec
                            g_hi = g_
                            k_hi = kg(n)
                            k_val = k_lo + (k_hi - k_lo) * (g_val - g_lo) / (g_hi - g_lo)
                            k_vec(m) = k_val

                            exit
                        end if
                    end do

                    n1 = n
                end do
            end if

            return
        end subroutine interpolate_k_vec
end module k_correlation