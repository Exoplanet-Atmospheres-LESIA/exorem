module cross_sections
    ! """
    ! Contains all the subroutine needed to calculate the absorption cross sections.
    ! """
    use mpi_utils

    implicit none

    contains
        subroutine calculate_species_absorption_cross_sections()
            ! """
            ! Calculate the species absorption cross sections.
            ! """
            use physics, only: cst_c, cst_h, cst_k, cst_p0, cst_R, cst_t_ref
            use spectrometrics, only: doppler_deviation_tolerance, min_total_interval_size, &
                wavenumber_min, wavenumber_max, wavenumber_step
            use species, only: cutoffs, electronic_states_degeneracies, electronic_states_wavenumbers, &
                get_ph3_absorption_cross_section_correction_factor, &
                intensities_min, line_shape, lines_files, molar_masses, n_broadenings_max, n_species, &
                n_broadenings_species, n_electronic_states_species, n_vibrational_modes_species, &
                rotational_partition_exponents, species_broadenings, species_broadening_temperature_coefficients, &
                species_names, species_vmr, temperature_intensities_min, vibrational_modes_wavenumbers, &
                vibrational_modes_degeneracies
            use thermodynamics, only: n_levels, pressure_space, size_thermospace, temperature_space
            use files, only: file_name_size
            use interface, only: absorption_cross_section_file_prefix, get_rey_ch4_lines_file, &
                output_species_by_species, path_outputs, read_lines_files, read_lines_files_ch4, read_voigt_file, &
                reallocate_2Ddouble, reallocate_2dinteger, use_profile_mode, voigt_data_file, &
                write_absorption_cross_sections_files
            use math, only: sqrtpi, voigt_from_data

            implicit none

            doubleprecision, parameter :: &
                hck = cst_h * cst_c * 1d2 / cst_k, &                    ! x 100 for conversion from m-1 to cm-1
                detuning_wavenumber_factor = 5d-2                       ! cutoff protion where to smooth pure Voigt pro.

            character(len=file_name_size) :: &
                lines_files_ch4(n_levels), &                            ! CH4 lines files
                lines_files_ch4_sl(n_levels)                            ! CH4 superlines files

            integer :: &
                i, &                                                    ! index
                i_broadening_line, &                                    ! broadening index of a spectral line
                i_t, &                                                  ! index (thermospace)
                j, &                                                    ! index
                j_min_step, &                                           ! level of minimal wavenumner step
                j_max_step, &                                           ! level of maximal wavenumber steo
                k, &                                                    ! index
                l, &                                                    ! index
                n, &                                                    ! index
                n_lines(n_species), &                                   ! number of lines for each species
                n_superlines, &                                         ! number of superlines (Rey 2017 CH4 only)
                n_wavenumbers_cutoff, &                                 ! nb of wvn from the center of the line to ctoff
                n_wavenumbers__(n_levels), &                            ! number of wavenumbers at each levels
                n_wavenumbers0, &                                       ! number of wavenumbers in spectral sub-interval
                n_wavenumbers_min, &                                    ! minimum number of wavenumbers
                spectral_interval, &                                    ! spectral interval number
                wavenumber_step_divider(n_levels)                       ! divider of the wavenumber step

            integer, dimension(:), allocatable :: &
                i_broadening_lines, &                                   ! indices of broadenings for each lines
                i_broadening_lines_ch4, &                               ! indices of broadenings for each CH4 lines
                i_broadening_superlines_ch4                             ! indices of broadenings for each CH4 superlines

            integer, dimension(:, :), allocatable :: &
                i_broadening_lines_species                              ! indices of broadenings for each lines

            doubleprecision :: &
                collision_hwhm, &                                       ! collision (Lorentzian) broadening HWHM
                correction_factor, &                                    ! absorption cross section correction factor
                detuning_wavenumber(n_species), &                       ! (cm-1) only to smooth pure Voigt profile
                doppler_hwhm, &                                         ! Doppler broadening Half Width Half Maximum
                hck_t_ref, &                                            ! = hc/k * 1/T_ref
                hckt, &                                                 ! = hc/k * 1/T
                levels_wavenumber_step(n_levels), &                     ! wavenumber step at each levels
                line_e_low, &                                           ! (cm-1) energy of the low tran. lvl of the line
                line_intensity, &                                       ! (cm-1/(molecules.cm-2)) intensity of the line
                line_intensity_min, &                                   ! (cm-1/(molecules.cm-2)) min int. of the line
                line_wavenumber, &                                      ! (cm-1) wavenumber of the line
                mpi_cut_factor, &                                       ! used to share the spectral interval (MPI)
                q_electronic_ref, &                                     ! reference electronic partition function
                ratio_q_electronic, &                                   ! ratio of the electronic partition functions
                ratio_q_rotation, &                                     ! ratio of the rotation partition functions
                ratio_q_total, &                                        ! ratio of all the partitions functions
                ratio_q_vibration, &                                    ! ratio of the vibration partition functions
                species_broadenings_borders(n_species, n_broadenings_max), &  ! species broadening borders
                sum_voigt, &                                            ! sum of the Voigt functions
                sum_voigt_khi, &                                        ! sum of the Voigt functions taking khi into acc
                t_ref, &                                                ! (K) reference temperature
                t_ref_ch4, &                                            ! (K) reference temperature
                temperatures(n_levels, size_thermospace), &             ! (K) temperatures
                voigt_data(400, 100), &                                 ! data used to construct the Voigt profile
                voigt0, &                                               ! Voigt profile at x = 0
                wavenumber_max__, &                                     ! (cm-1) max wavenumber of the spec. interval
                wavenumber_max_bcasted__, &                             ! (cm-1) max wvn broadcasted to each processes
                wavenumber_mid, &                                       ! (cm-1) wvn at the middle of a sub-interval
                wavenumber_min__, &                                     ! (cm-1) min wavenumber of the spectral interval
                wvn_max, &                                              ! (cm-1) max wvn of the spec. sub-interval
                wvn_min(n_levels), &                                    ! (cm-1) min wvn of the spec. sub-interval
                wvn_step_multiplicator = 1d0, &                         ! wavenumber step multiplicator
                x, &                                                    ! x of the Voigt function
                y                                                       ! y of the Voigt function

            doubleprecision, dimension(:), allocatable :: &
                absorption_cross_sections_k, &                          ! (cm2) absorption cross sections at an atm lvl
                ch4_lines_e_low, &                                      ! (cm-1) energy of the low tran lvl of CH4 lines
                ch4_lines_intensity, &                                  ! (cm-1/(molecules.cm-2)) intensity of CH4 lines
                ch4_lines_wavenumber, &                                 ! (cm-1) wavenumber of CH4 lines
                ch4_superlines_intensity, &                             ! (cm-1/(molecules.cm-2)) int of CH4 superlines
                ch4_superlines_wavenumber, &                            ! (cm-1) wavenumber of CH4 superlines
                lines_e_low, &                                          ! (cm-1) energy of the low tran. lvl of the line
                lines_intensity, &                                      ! (cm-1/(molecules.cm-2)) intensity of the lines
                lines_wavenumber                                        ! (cm-1) wavenumber of the lines

            doubleprecision, dimension(:, :), allocatable :: &
                f_voigt, &                                              ! Voigt function
                lines_e_low_species, &                                  ! (cm-1) energy of the low tran. lvl of the line
                lines_intensity_species, &                              ! (cm-1/(molecules.cm-2)) intensity of the lines
                lines_wavenumber_species                                ! (cm-1) wavenumber of the lines

            doubleprecision, dimension(:, :), allocatable :: &
                absorption_cross_sections__                             ! (cm2) absorption cross sections

            write(mpi_msg, '("Calculating absorption cross sections...")')
            call mpi_print(mpi_msg, 0)

            ! Share the spectral interval among the processes
            call share_wavenumbers()

            ! Display info
            write(mpi_msg, '(" Spectrum is computed from", f9.3, " to ", f9.3, " cm-1", A, &
                &" Calculation step is", f6.1, " times the smallest line halfwidth")') &
                wavenumber_min, wavenumber_max, NEW_LINE('A'), wvn_step_multiplicator
            call mpi_print(NEW_LINE('A') // mpi_msg, 0)

            ! Read the Voigt data
            call read_voigt_file(voigt_data_file, voigt_data)

            ! Calculate broadening blocks borders
            allocate(&
                lines_wavenumber_species(n_levels, 1), &
                lines_intensity_species(n_levels, 1), &
                lines_e_low_species(n_levels, 1), &
                i_broadening_lines_species(n_levels, 1) &
            )

            lines_wavenumber_species(:, :) = 0d0
            lines_intensity_species(:, :) = 0d0
            lines_e_low_species(:, :) = 0d0
            i_broadening_lines_species(:, :) = 0
            n_lines(:) = -1
            detuning_wavenumber(:) = 0d0

            do k = 1, n_species
                if (trim(species_names(k)) == 'Na' .or. trim(species_names(k)) == 'K'.or.trim(species_names(k))=='test')  then
                    if (trim(line_shape(k)) == 'Burrows') then
                        write(mpi_msg, '("Using Burrows profile for alkalis")')
                        call mpi_print(mpi_msg, 0)
                    else if (trim(line_shape(k)) == 'Voigt') then
                        write(mpi_msg, '("Using pure Voigt profile for alkalis, using Burrows is recommended")')
                        call mpi_print_warning(mpi_msg, 0)

                        detuning_wavenumber(k) = cutoffs(k) * detuning_wavenumber_factor
                        cutoffs(k) = cutoffs(k) + detuning_wavenumber(k)
                    else
                        write(mpi_msg, '("Shape ''", A, "'' is not implemented for alkali ''", A, "''")') &
                            trim(line_shape(k)), trim(species_names(k))
                        call mpi_print_error(mpi_msg, 0)
                    end if
                else
                    if (trim(line_shape(k)) == 'VoigtKhi') then
                        write(mpi_msg, '("Using Voigt profile with khi factor for molecule ''", A, "''")') &
                            trim(species_names(k))
                        call mpi_print(mpi_msg, 0)
                    else if (trim(line_shape(k)) == 'Voigt') then
                        write(mpi_msg, '("Using pure Voigt profile for species''", A, &
                            &"'', using VoigtKhi is recommended")') trim(species_names(k))
                        call mpi_print_warning(mpi_msg, 0)

                        detuning_wavenumber(k) = cutoffs(k) * detuning_wavenumber_factor
                        cutoffs(k) = cutoffs(k) + detuning_wavenumber(k)
                    else
                        write(mpi_msg, '("Shape ''", A, "'' is not implemented for molecule ''", A, "''")') &
                            trim(line_shape(k)), trim(species_names(k))
                        call mpi_print_error(mpi_msg, 0)
                    end if
                end if

                if(n_broadenings_species(k) <= 0)  then
                    write(mpi_msg, '("Using directly lines broadening data for species ''", A, "''")') &
                        trim(species_names(k))
                    call mpi_print_info(mpi_msg, 0)
                    write(mpi_msg, '("Not implemented yet !")') &  ! TODO use directly lines broadening instead of broadening blocks
                        trim(species_names(k))
                    call mpi_print_error(mpi_msg, 0)
                else
                    do i = 1, n_broadenings_species(k) - 1
                        species_broadenings_borders(k, i) = &
                            (species_broadenings(k, i) + species_broadenings(k, i + 1)) / 2
                    end do
                end if

                if(trim(species_names(k)) /= 'CH4') then
                    write(mpi_msg, '(1X, "Reading lines of species ''", A, "'' in file ''", A, "''")') &
                        trim(species_names(k)), trim(lines_files(k))
                    call mpi_print(mpi_msg, mpi_rank)

                    call read_lines_files(lines_files(k), &
                        wavenumber_min__ - cutoffs(k), wavenumber_max__ + cutoffs(k), &
                        intensities_min(k), temperature_intensities_min(k), &
                        rotational_partition_exponents(k), &
                        n_broadenings_species(k), species_broadenings_borders(k, :), &
                        lines_wavenumber, lines_intensity, lines_e_low, i_broadening_lines, &
                        n_lines(k))

                    if(n_lines(k) == maxval(n_lines(:))) then
                        call reallocate_2Ddouble(lines_wavenumber_species, n_species, maxval(n_lines(:)))
                        call reallocate_2Ddouble(lines_intensity_species, n_species, maxval(n_lines(:)))
                        call reallocate_2Ddouble(lines_e_low_species, n_species, maxval(n_lines(:)))
                        call reallocate_2Dinteger(i_broadening_lines_species, n_species, maxval(n_lines(:)))
                    end if

                    lines_wavenumber_species(k, :n_lines(k)) = lines_wavenumber(:)
                    lines_intensity_species(k, :n_lines(k)) = lines_intensity(:)
                    lines_e_low_species(k, :n_lines(k)) = lines_e_low(:)
                    i_broadening_lines_species(k, :n_lines(k)) = i_broadening_lines(:)
                end if
            end do

            ! Initialise the temperatures
            if (use_profile_mode) then
                temperatures(:, 1) = temperature_space(:)  ! TODO improve this
            else
                do i_t = 1, size_thermospace
                    temperatures(:, i_t) = temperature_space(i_t)
                end do
            end if

            ! Loop over each thermodynamic space
            do i_t = 1, size_thermospace
                write(mpi_msg, '("Thermodynamic space ", I0, "/", I0)') i_t, size_thermospace
                call mpi_print(NEW_LINE('A') // mpi_msg, 0)

                ! Initialise loop values
                wvn_max = 0d0
                wvn_min(:) = wavenumber_min__
                spectral_interval = 0

                ! Loop over the spectral sub-intervals
                do while(wvn_max < wavenumber_max__)
                    ! Determination of the calculation step (cm-1)
                    spectral_interval = spectral_interval + 1

                    ! Get the calculation step to get the upper value of the wavenumber interval
                    if(i_t == 1 .and. spectral_interval == 1) then  ! TODO warning: i_t == 1 is because this is where there is the coldest temperature of the profiles
                        call get_synthetic_spectrum_wavenumber_step(&
                            wavenumber_min__, pressure_space(:), temperatures(:, i_t), wvn_step_multiplicator, &
                            levels_wavenumber_step(:), wavenumber_step_divider(:) &
                        )
                    end if

                    ! Get the level where the wavenumber step is minimal and maximal
                    j_min_step = minloc(levels_wavenumber_step(:), dim=1)
                    j_max_step = maxloc(levels_wavenumber_step(:), dim=1)

                    ! Initialise the spectral sub-interval
                    n_wavenumbers0 = min(&
                        wavenumber_step_divider(j_min_step) * &
                        (floor((wavenumber_max__ - wvn_min(j_min_step)) / wavenumber_step) + 1), &
                        wavenumber_step_divider(j_min_step) * &
                        (floor(wvn_min(j_min_step) * doppler_deviation_tolerance / wavenumber_step) + 1))
                    n_wavenumbers_min = n_wavenumbers0 / wavenumber_step_divider(j_min_step)
                    wvn_max = wvn_min(j_max_step) + &
                        (n_wavenumbers0 - wavenumber_step_divider(j_min_step)) * levels_wavenumber_step(j_min_step)

                    ! Safety condition
                    if(wvn_max > wavenumber_max__) then
                        wvn_max = wavenumber_max__
                    end if

                    ! Display info
                    write(mpi_msg, &
                        '(A, "Proc. ", I0.3, " Spectral Interval ", I0, ", Thermodynamic Space ", I0, "/", I0, A, &
                        &" Calculation from ", F0.6, " to ", F0.6, " cm-1 with a minimum step of", ES11.4, " cm-1 &
                        &(/", I0, ")")') &
                        NEW_LINE('A'), mpi_rank, spectral_interval, i_t, size_thermospace, NEW_LINE('A'),&
                        wvn_min(j_min_step), wvn_max, &
                        levels_wavenumber_step(j_min_step), wavenumber_step_divider(j_min_step)
                    call mpi_print(mpi_msg, mpi_rank)

                    ! Get the wavenumber at the middle of the interval
                    wavenumber_mid = 0.5d0 * (wvn_min(j_min_step) + wvn_max)

                    ! Initialise the number of wavenumbers
                    if(spectral_interval == 1) then
                        do j = 1, n_levels
                            n_wavenumbers__(j) = wavenumber_step_divider(j) * (n_wavenumbers_min - 1) + 1
                        end do
                    else
                        do j = 1, n_levels
                            n_wavenumbers__(j) = wavenumber_step_divider(j) * n_wavenumbers_min
                        end do
                    end if

                    ! Allocate absorption cross sections
                    if(allocated(absorption_cross_sections__)) then
                        deallocate(absorption_cross_sections__)
                    end if

                    allocate(&
                        absorption_cross_sections__(n_levels, maxval(wavenumber_step_divider(:) * n_wavenumbers_min)) &
                    )

                    absorption_cross_sections__(:, :) = 0d0

                    ! Loop over the species
                    do k = 1, n_species
                        ! Loop over the atmospheric levels
                        do j = n_levels, 1, -1
                            ! Read CH4 spectroscopic files (Rey et al.)
                            if (trim(species_names(k)) == 'CH4' .and. spectral_interval == 1 .and. &
                                (use_profile_mode .or. (.not. use_profile_mode .and. j == n_levels))) then
                                call get_rey_ch4_lines_file(temperatures(j, i_t), wavenumber_max__, &
                                    lines_files_ch4(j), lines_files_ch4_sl(j), t_ref_ch4)

                                write(mpi_msg, '(1X, "Reading CH4 lines in file ''", A, "'' and superlines &
                                        &in file ''", A,"'' (T_level = ", F0.1, " K, T_ref = ", F0.1, " K)")') &
                                    trim(lines_files_ch4(j)), trim(lines_files_ch4_sl(j)), &
                                    temperatures(j, i_t), t_ref_ch4
                                call mpi_print(mpi_msg, 0)

                                call read_lines_files_ch4(lines_files_ch4(j), lines_files_ch4_sl(j), &
                                    wavenumber_min__ - cutoffs(k), wavenumber_max__ + cutoffs(k), &
                                    n_broadenings_species(k), species_broadenings_borders(k, :), &
                                    ch4_lines_wavenumber, ch4_lines_intensity, ch4_lines_e_low, &
                                    i_broadening_lines_ch4, i_broadening_superlines_ch4, &
                                    n_lines(k), &
                                    ch4_superlines_wavenumber, ch4_superlines_intensity, n_superlines)
                            end if

                            ! Pre calculations
                            hckt = hck / temperatures(j, i_t)

                            ! Correct lack of completeness of linelist at high temperature
                            if(trim(species_names(k)) == 'PH3') then
                                correction_factor = &
                                    get_ph3_absorption_cross_section_correction_factor(temperatures(j, i_t))
                            else
                                correction_factor = 1d0
                            end if

                            ! Initialise f_voigt
                            n_wavenumbers_cutoff = nint(cutoffs(k) / levels_wavenumber_step(j)) + 1

                            if(allocated(f_voigt)) then
                                deallocate(f_voigt)
                            end if

                            allocate(f_voigt(n_broadenings_max, n_wavenumbers_cutoff))
                            f_voigt(:, :) = 0d0

                            ! Initialise tha absorption cross sections
                            if(allocated(absorption_cross_sections_k)) then
                                deallocate(absorption_cross_sections_k)
                            end if

                            allocate(absorption_cross_sections_k(n_wavenumbers__(j)))
                            absorption_cross_sections_k(:) = 0d0

                            ! Initialise reference temperature
                            if(trim(species_names(k)) /= 'CH4') then
                                t_ref = cst_t_ref
                            else
                                t_ref = t_ref_ch4
                            end if

                            hck_t_ref = hck / t_ref

                            ! Calculate the rotational partition coefficient ratio
                            ratio_q_rotation = (t_ref / temperatures(j, i_t)) ** rotational_partition_exponents(k)

                            ! Calculate the vibrational partition coefficient ratio
                            ratio_q_vibration = 1d0

                            if(n_vibrational_modes_species(k) > 0) then
                                do i = 1, n_vibrational_modes_species(k)
                                    ratio_q_vibration = ratio_q_vibration * (&
                                            (1d0 - exp(-vibrational_modes_wavenumbers(k, i) * hckt)) / &
                                            (1d0 - exp(-vibrational_modes_wavenumbers(k, i) * hck_t_ref)) &
                                        ) ** vibrational_modes_degeneracies(k, i)
                                end do
                            end if

                            ! Calculate the electronic partition coefficient ratio
                            if(n_electronic_states_species(k) > 0) then
                                ratio_q_electronic = 0d0
                                q_electronic_ref = 0d0

                                do i = 1, n_electronic_states_species(k)
                                    q_electronic_ref = q_electronic_ref + electronic_states_degeneracies(k, i) * &
                                        exp(-electronic_states_wavenumbers(k, i) * hck_t_ref)
                                    ratio_q_electronic = ratio_q_electronic + electronic_states_degeneracies(k, i) * &
                                        exp(-electronic_states_wavenumbers(k, i) * hckt)
                                end do

                                ratio_q_electronic = q_electronic_ref / ratio_q_electronic
                            else
                                ratio_q_electronic = 1d0
                            end if

                            ! Doppler width of the lines
                            doppler_hwhm = wavenumber_mid * &
                                sqrt(2d0 * cst_R * temperatures(j, i_t) / molar_masses(k)) / cst_c

                            ! Get the Voigt profiles
                            do n = 1, n_broadenings_species(k)
                                ! Lorentz halfwidth (cm-1)
                                collision_hwhm = species_broadenings(k, n) * &
                                    (pressure_space(j) / cst_p0) * &
                                    (cst_t_ref / temperatures(j, i_t)) ** &
                                    species_broadening_temperature_coefficients(k, n)
                                y = collision_hwhm / doppler_hwhm
                                voigt0 = voigt_from_data(0d0, y, voigt_data)

                                if (trim(species_names(k)) == 'Na' .or. trim(species_names(k)) == 'K') then
                                    if (trim(line_shape(k)) == 'Burrows') then
                                        call get_f_voigt_alkali()
                                    else
                                        call get_pure_f_voigt()
                                    end if
                                else
                                    if (trim(line_shape(k)) == 'VoigtKhi') then
                                        call get_f_voigt_molecules()
                                    else
                                        call get_pure_f_voigt()
                                    end if
                                end if
                            end do

                            ! Re-calculate lines intensities taking into account the species quantum levels
                            line_intensity_min = intensities_min(k) / &
                                (ratio_q_rotation * ratio_q_vibration * ratio_q_electronic)

                            ! Shrink the lines parameter arrays by filtering them using the new intensity criterion
                            do l = 1, n_lines(k)
                                ! Temporary store lines parameters
                                if(trim(species_names(k)) == 'CH4') then
                                    line_wavenumber = ch4_lines_wavenumber(l)
                                    line_e_low = ch4_lines_e_low(l)
                                    line_intensity = ch4_lines_intensity(l) * &
                                        exp(-line_e_low *(hckt - hck_t_ref)) * &
                                        (1d0 - exp(-line_wavenumber * hckt)) / &
                                        (1d0 - exp(-line_wavenumber * hck_t_ref))  ! QStimulation ratio
                                else
                                    line_wavenumber = lines_wavenumber_species(k, l)
                                    line_e_low = lines_e_low_species(k, l)
                                    line_intensity = lines_intensity_species(k, l) * &
                                        exp(-line_e_low * (hckt - hck_t_ref)) * &
                                        (1d0 - exp(-line_wavenumber * hckt)) / &
                                        (1d0 - exp(-line_wavenumber * hck_t_ref))  ! QStimulation ratio
                                end if

                                ! Use the new intensity criterion as a line filter
                                if(line_intensity > line_intensity_min) then
                                    if(trim(species_names(k)) == 'CH4') then
                                        i_broadening_line = i_broadening_lines_ch4(l)
                                    else
                                        i_broadening_line = i_broadening_lines_species(k, l)
                                    end if

                                    ! Calculate the absorption cross sections of the line
                                    call get_line_absorption_cross_sections(&
                                        line_wavenumber, line_intensity, i_broadening_line, f_voigt(:, :), wvn_min(j), &
                                        wvn_max, levels_wavenumber_step(j), n_wavenumbers__(j), &
                                        cutoffs(k), n_wavenumbers_cutoff, absorption_cross_sections_k(:) &
                                    )
                                end if
                            end do

                            ! Calculate the total partition functions ratio
                            ratio_q_total = ratio_q_rotation * ratio_q_vibration * ratio_q_electronic

                            ! Calculate the actual absorption cross sections
                            do i = 1, n_wavenumbers__(j)
                                absorption_cross_sections__(j, i) = absorption_cross_sections__(j, i) + &
                                    absorption_cross_sections_k(i) * &
                                    ratio_q_total * species_vmr(k) * sqrtpi / doppler_hwhm
                            end do

                            ! If the species is CH4, add the contribution of the super lines
                            if(trim(species_names(k)) == 'CH4') then
                                ! Re-initialise absorption cross sections
                                absorption_cross_sections_k(:) = 0d0

                                do l = 1, n_superlines
                                    call get_line_absorption_cross_sections(&
                                        ch4_superlines_wavenumber(l), ch4_superlines_intensity(l), &
                                        i_broadening_superlines_ch4(l), f_voigt(:, :), wvn_min(j), &
                                        wvn_max, levels_wavenumber_step(j), n_wavenumbers__(j), &
                                        cutoffs(k), n_wavenumbers_cutoff, absorption_cross_sections_k(:) &
                                    )
                                end do

                                do i = 1, n_wavenumbers__(j)
                                    absorption_cross_sections__(j, i) = absorption_cross_sections__(j, i) + &
                                        absorption_cross_sections_k(i) * sqrtpi * species_vmr(k) / doppler_hwhm
                                end do
                            end if

                            ! Apply correction factor
                            absorption_cross_sections__(j, :) = absorption_cross_sections__(j, :) / correction_factor

                            ! Write the absorption cross sections in the output file
                            if (output_species_by_species) then
                                call write_absorption_cross_sections_files(&
                                    species_names(k), wvn_min(j), wvn_max, n_wavenumbers__(j), &
                                    wavenumber_min__, wavenumber_max__, &
                                    temperatures(j, i_t), pressure_space(j), absorption_cross_sections__(j, :)&
                                )

                                if(allocated(absorption_cross_sections__)) then
                                    deallocate(absorption_cross_sections__)
                                end if

                                allocate(&
                                    absorption_cross_sections__(n_levels, &
                                        maxval(wavenumber_step_divider(:) * n_wavenumbers_min)) &
                                )

                                absorption_cross_sections__(:, :) = 0d0
                            end if
                        end do  ! n_levels
                    end do  ! n_species

                    ! Alternative output
                    if(.not. output_species_by_species) then
                        do j = 1, n_levels
                                call write_absorption_cross_sections_files(&
                                    species_names(1), wvn_min(j), wvn_max, n_wavenumbers__(j), &  ! TODO imrpove species name
                                    wavenumber_min__, wavenumber_max__, &
                                    temperatures(j, i_t), pressure_space(j), absorption_cross_sections__(j, :)&
                                )
                        end do
                    end if

                    ! Update loop values
                    do j = 1, n_levels
                        wvn_min(j) = wvn_max + levels_wavenumber_step(j)
                    end do

                    ! Safety while breaking condition
                    if(wvn_min(j_min_step) > wavenumber_max__) then
                        exit
                    end if
                end do
            end do

            ! Everything is fine :)
            write(mpi_msg, '("proccess", I0.3,": done !")') mpi_rank
            call mpi_print(mpi_msg, mpi_rank)

            ! Write process info file
            call write_processes_info(wavenumber_min__, wavenumber_max__, species_names(1))

            contains
                subroutine get_pure_f_voigt()
                    ! """
                    ! Get a pure Voigt line shape.
                    ! """
                    implicit none

                    integer :: n_voigt
                    doubleprecision :: d_nu, start_smooth

                    start_smooth = cutoffs(k) - 2d0 * detuning_wavenumber(k)
                    n_voigt = int(start_smooth / levels_wavenumber_step(j)) + 1

                    do i = 1, n_voigt
                        x = levels_wavenumber_step(j) * (i - 1) / doppler_hwhm
                        f_voigt(n, i) = voigt_from_data(x, y, voigt_data)
                    end do

                    ! Smooth the tail of the line profile
                    do i = n_voigt + 1, n_wavenumbers_cutoff
                        x = levels_wavenumber_step(j) * (i - 1) / doppler_hwhm
                        d_nu = levels_wavenumber_step(j) * (i - 1)
                        f_voigt(n, i) = voigt_from_data(x, y, voigt_data) * &
                            exp(-((d_nu - start_smooth) / detuning_wavenumber(k)) ** 2d0)
                    end do
                end subroutine get_pure_f_voigt

                subroutine get_f_voigt_alkali()
                    ! """
                    ! Get the line shape of alkali species.
                    ! """
                    implicit none

                    integer :: &
                        n_voigt     ! number of wavenumbers where to apply the Voigt profile

                    doubleprecision :: &
                        detuning, &
                        efactor

                    detuning = 0d0
                    efactor = 0d0

                    if (trim(species_names(k)) == 'Na') then
                        detuning = 3d1 * (temperatures(j, i_t) / 5d2) ** 0.6
                        efactor = 5d3
                    else if (trim(species_names(k)) == 'K') then
                        detuning = 2d1 * (temperatures(j, i_t) / 5d2) ** 0.6
                        efactor = 1.6d3
                    end if

                    n_voigt = int(detuning / levels_wavenumber_step(j)) + 1

                    do i = 1, n_voigt
                        x = levels_wavenumber_step(j) * (i - 1) / doppler_hwhm
                        f_voigt(n, i) = voigt_from_data(x, y, voigt_data)
                    end do

                    do i = n_voigt + 1, n_wavenumbers_cutoff
                        x = levels_wavenumber_step(j) * (i - 1)
                        f_voigt(n, i) = f_voigt(n, n_voigt) * &
                            (detuning / x) ** 1.5 * exp(-hck * x ** 2 / (efactor * temperatures(j, i_t)))
                    end do
                end subroutine get_f_voigt_alkali

                subroutine get_f_voigt_molecules()
                    ! """
                    ! Get the line shape of molecules.
                    ! """
                    implicit none

                    integer, parameter :: size_khi = 3
                    integer :: i_khi
                    doubleprecision :: voigt_factor, &
                        khi_factors_a(size_khi), khi_factors_b(size_khi), khi_limits(size_khi), d_nu

                    sum_voigt = 0.5d0 * voigt0
                    sum_voigt_khi = 0.5d0 * voigt0
                    voigt_factor = 0d0

                    f_voigt(n, :) = tiny(0.) * voigt0

                    i_khi = 1

                    if(trim(species_names(1)) == 'CO2') then
                        ! Khi factor for CO2 (Burch et al. 1969)
                        khi_factors_a = [2.5825d0, 0.1804d0, 0.3131d-1]
                        khi_factors_b = [1.054d0, 1.628d1, 8.761d1]
                        khi_limits = [1d0, 3d0, 35d0]  ! cm-1
                    else  ! all the other species use the khi factor for CH4
                        ! Khi factor for CH4 (Hartmann et al. 2002)
                        khi_factors_a = [8.72d0, 0.0684d0, 0.0684d0]
                        khi_factors_b = [12d0, 393d0, 393d0]
                        khi_limits = [26d0, 60d0, huge(0d0)]  ! cm-1
                    end if

                    do i = 1, n_wavenumbers_cutoff
                        x = levels_wavenumber_step(j) * (i - 1) / doppler_hwhm
                        f_voigt(n, i) = voigt_from_data(x, y, voigt_data)

                        if(i > 1) then
                            sum_voigt = sum_voigt + f_voigt(n, i)
                        end if

                        if(f_voigt(n, i) < tiny(0.) * voigt0 .and. &
                           n == n_broadenings_species(k)) then
                            n_wavenumbers_cutoff = i

                            exit
                        end if

                        d_nu = levels_wavenumber_step(j) * dble(i - 1)

                        if (d_nu < khi_limits(1)) then
                            voigt_factor = 1d0
                        else
                            if (d_nu > khi_limits(min(i_khi + 1, size_khi))) then
                                i_khi = i_khi + 1
                            end if

                            voigt_factor = khi_factors_a(i_khi) * exp(-d_nu / khi_factors_b(i_khi))
                        end if

                        f_voigt(n, i) = f_voigt(n, i) * voigt_factor

                        if(i > 1) then
                            sum_voigt_khi = sum_voigt_khi + f_voigt(n, i)
                        end if
                    end do

                    do i = 1, n_wavenumbers_cutoff
                        f_voigt(n, i) = f_voigt(n, i) * sum_voigt / sum_voigt_khi
                    end do
                end subroutine get_f_voigt_molecules

                subroutine share_wavenumbers()
                ! """
                ! Share the wavenumbers among the processes, following a logarithmic rule.
                ! This is done by modifying wavenumber min and max of each processes.
                ! The wavenumber interval of a process cannot be lower than the minimum interval size set in input.
                ! Process n have always its first wavenumber in common with process n - 1.
                ! """
                implicit none

                if(wavenumber_max - wavenumber_min < min_total_interval_size * mpi_nproc) then
                    write(mpi_msg, &
                        '("[MPI] spectral interval (", F0.3, "-", F0.3, " cm-1) cannot be divided into chunks &
                        &of at least ", F0.3, "cm-1 ; increase the spectral interval, decrease the minimum interval &
                        &size set in the input file, or decrease the number of processes")') &
                        wavenumber_min, wavenumber_max, min_total_interval_size
                    call mpi_print_error(mpi_msg, 0)
                end if

                mpi_cut_factor = exp(log(wavenumber_max / wavenumber_min) / mpi_nproc)

                do i = 1, mpi_nproc
                    if(mpi_rank == i - 1) then
                        if(mpi_rank == 0) then
                            wavenumber_min__ = wavenumber_min
                        else
                            wavenumber_min__ = wavenumber_max_bcasted__
                        end if

                        if (mpi_rank == mpi_nproc - 1) then
                            wavenumber_max__ = wavenumber_max
                        else
                            wavenumber_max__ = mod(wavenumber_min, min_total_interval_size) + &
                                dble(&
                                floor((wavenumber_min * mpi_cut_factor ** (mpi_rank + 1)) / min_total_interval_size) &
                                ) * min_total_interval_size

                            if (wavenumber_max__ - wavenumber_min__ < min_total_interval_size) then
                                wavenumber_max__ = wavenumber_min__ + min_total_interval_size
                            end if
                        end if
                    end if

                    wavenumber_max_bcasted__ = wavenumber_max__

                    call MPI_BCAST(wavenumber_max_bcasted__, 1, MPI_DOUBLE_PRECISION, i - 1, MPI_COMM_WORLD, mpi_ierr)
                end do
            end subroutine share_wavenumbers
        end subroutine calculate_species_absorption_cross_sections


        subroutine get_synthetic_spectrum_wavenumber_step(&
            wavenumber, pressures, temperatures, wvn_step_multiplicator, &
            levels_wavenumber_step, wavenumber_step_dividers &
        )
            ! """
            ! Get the synthetic spectrum wavenumber step.
            ! The wavenumber step at one level is based on the minimal line width amongst all the modelised species
            ! at each the atmospheric level.
            ! :param wavenumber: (cm-1) reference wavenumber to calculate the step
            ! :param pressures: (Pa) pressures explored during the calculation
            ! :param temperatures: (K) temperatures explored during the calculation
            ! :param wvn_step_multiplicator: multiply the final wavenumber step by this value
            ! :return levels_wavenumber_step: (cm-1) wavenumber step at each level
            ! :return wavenumber_step_dividers: how much the reference wavenumber step has been divided at each level
            ! """
            use physics, only: cst_c, cst_R, cst_p0, cst_t_ref
            use species, only: n_species, species_broadenings, species_broadening_temperature_coefficients, &
                molar_masses
            use spectrometrics, only: wavenumber_step
            use thermodynamics, only: n_levels

            implicit none

            doubleprecision, intent(in) :: wavenumber, temperatures(n_levels), pressures(n_levels), &
                wvn_step_multiplicator

            integer, intent(out) :: wavenumber_step_dividers(n_levels)
            doubleprecision, intent(out) :: levels_wavenumber_step(n_levels)

            integer :: &
                i_min_broadenings(n_species), &     ! indices of the minimum broadenings of each species
                j, &                                ! index
                k                                   ! index

            doubleprecision :: &
                collision_hwhm, &                   ! (cm-1) Lorentz halfwidth
                doppler_hwhm, &                     ! (cm-1) Doppler halfwidth
                line_hwhm, &                       ! (cm-1) line profile halfwidth
                line_hwhm_min                      ! (cm-1) minimum line profile halfwidth

            ! Initialisation
            line_hwhm_min = huge(0.)
            i_min_broadenings(:) = minloc(species_broadenings(:, :), dim=2)

            ! Find the minimum line width of each species, amongst all the atmospheric levels
            do j = 1, n_levels
                do k = 1, n_species
                    doppler_hwhm = wavenumber * &
                        sqrt(2d0 * log(2d0) * cst_R * temperatures(j) / molar_masses(k)) / cst_c
                    collision_hwhm = species_broadenings(k, i_min_broadenings(k)) * (pressures(j) / cst_p0) * &
                        (cst_t_ref / temperatures(j)) ** &
                        species_broadening_temperature_coefficients(k, i_min_broadenings(k))
                    line_hwhm = sqrt(doppler_hwhm ** 2 + collision_hwhm ** 2)

                    if(line_hwhm < line_hwhm_min) then
                        line_hwhm_min = line_hwhm
                    end if
                end do

                levels_wavenumber_step(j) = line_hwhm_min * wvn_step_multiplicator
                wavenumber_step_dividers(j) = 1 + int(wavenumber_step / levels_wavenumber_step(j))
                levels_wavenumber_step(j) = wavenumber_step / dble(wavenumber_step_dividers(j))
            end do
        end subroutine get_synthetic_spectrum_wavenumber_step


        subroutine get_line_absorption_cross_sections(&
            line_wavenumber, line_intensity, i_broadening_line, f_voigt, &
            wavenumber_min, wavenumber_max, wavenumber_step, n_wavenumbers, cutoff, n_wavenumbers_cutoff, &
            absorption_cross_sections &
        )
            ! """
            ! Get the absorption cross section of a line with Voigt profile.
            ! There must be one Voigt profile per line broadening, and the Voigt profile must be calculated from the
            ! center of the line to the cutoff.
            ! :param line_wavenumber: (cm-1) wavenumber of the line
            ! :param line_intensity: (cm-1/(molecules.cm-2)) intensity of the line
            ! :param i_broadening_line: line broadening index
            ! :param f_voigt: Voigt function of shape (n_broadening, n_wavenumbers_cutoff)
            ! :param wavenumber_min: (cm-1) minimum wavenumber of the spectral interval
            ! :param wavenumber_max: (cm-1) maximum wavenumber of the spectral interval
            ! :param wavenumber_step: (cm-1) step between to wavenumbers of the wavenumber interval
            ! :param n_wavenumbers: number of wavenumbers in the wavenumber interval
            ! :param cutoff: (cm-1) distance from the line center where the line is no more calculated
            ! :param n_wavenumbers_cutoff: number of wavenumbers from the center of the line profile to the cutoff
            ! :return absorption_cross_sections: (cm2) absorption cross section of the line
            ! """
            implicit none

            integer, intent(in) :: n_wavenumbers, n_wavenumbers_cutoff, i_broadening_line
            doubleprecision, intent(in) :: line_wavenumber, line_intensity, &
                wavenumber_min, wavenumber_max, wavenumber_step, cutoff
            doubleprecision, dimension(:, :), intent(in) :: f_voigt

            doubleprecision, intent(out) :: absorption_cross_sections(n_wavenumbers)

            integer :: &
                i, &                ! index
                i_line_center, &    ! index of the center of the line profile in the wavenumber interval
                i_line_min, &       ! index of the left cutoff of the line profile in the wavenumber interval
                i_line_max, &       ! index of the right cutoff of the line profile in the wavenumber interval
                i_min, &            ! minimum index of the line in the wavenumber interval where to calculate the abs.
                i_mid, &            ! index separating the left and right sides of the line profile in the wvn. int.
                i_max               ! maximum index of the line in the wavenumber interval where to calculate the abs.

            ! Exclude lines too far from the wavenumber interval
            if(line_wavenumber > wavenumber_max + cutoff) then
                return
            elseif(line_wavenumber < wavenumber_min - cutoff) then
                return
            end if

            ! Find the index of the wavenumber interval where the line center is located
            i_line_center = nint((line_wavenumber - wavenumber_min) / wavenumber_step) + 1

            ! Find the index of the borders of the line profile
            i_line_min = i_line_center - n_wavenumbers_cutoff + 1
            i_line_max = i_line_center + n_wavenumbers_cutoff - 1

            ! Find the absorption cross section calculation indices
            ! 0 <= i_mid <= n + 1 (and not 1 <= i_mid <= n) because we need to calculate at i = 1 and i = n
            i_min = max(i_line_min, 1)
            i_mid = min(max(i_line_center, 0), n_wavenumbers + 1)
            i_max = min(i_line_max, n_wavenumbers)

            ! Left side of the line profile (center of the line not included)
            if(i_line_center > 1) then  ! line_wavenumber > wavenumber_min
                do i = i_min, i_mid - 1
                    absorption_cross_sections(i) = absorption_cross_sections(i) + &
                        line_intensity * f_voigt(i_broadening_line, i_line_center - i + 1)
                end do
            end if

            ! Right side of the line profile (center of the line not included)
            if(i_line_center < n_wavenumbers) then  ! line_wavenumber < wavenumber_max
                do i = i_mid + 1, i_max
                    absorption_cross_sections(i) = absorption_cross_sections(i) + &
                        line_intensity * f_voigt(i_broadening_line, i - i_line_center + 1)
                end do
            end if

            ! Center of the line
            if(i_line_center >= 1 .and. i_line_center <= n_wavenumbers) then
                absorption_cross_sections(i_mid) = absorption_cross_sections(i_mid) + &
                    line_intensity * f_voigt(i_broadening_line, 1)
            end if
        end subroutine get_line_absorption_cross_sections


        subroutine write_processes_info(wavenumber_min__, wavenumber_max__, species_name)
            ! """
            ! Write the time-to-complete and wavenumber interval of each processes.
            ! """
            use files, only: file_name_size
            use interface, only: path_outputs, time_begin, time_end
            use species, only: species_name_size
            use spectrometrics, only: wavenumber_min, wavenumber_max

            implicit none

            doubleprecision, intent(in) :: wavenumber_min__, wavenumber_max__

            integer :: file_unit

            character(len=species_name_size) :: &
                species_name

            character(len=file_name_size) :: &
                file

            character(len=25) :: &
                title           ! the first 25 character of the fist line of the file

            integer :: &
                i               ! index

            call cpu_time(time_end)
            call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)

            write(file, '(A, ES9.3, A, ES9.3, A)') &
                trim(path_outputs) // 'processes_infos_' // trim(species_name) // '_', &
                wavenumber_min, '-', wavenumber_max, 'cm-1.dat'
            write(title, '(A)') 'n (s) (cm-1) (cm-1)'

            write(mpi_msg, '("proccess", I0.3,": done in ", F0.3," s !")') mpi_rank, time_end - time_begin
            call mpi_print(mpi_msg, mpi_rank)

            do i = 1, mpi_nproc
                if(mpi_rank == 0 .and. i == 1) then
                    open(newunit=file_unit, file=file, status='unknown')

                    write(file_unit, '(A25, I10, A)') &
                        title, mpi_nproc, &
                        ' | columns: rank of process, time to complete, wavenumber min, wavenumber max; species: ' // &
                        trim(species_name)

                    close(file_unit)
                end if

                call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)

                if(i - 1 == mpi_rank) then
                    open(newunit=file_unit, file=file, status='unknown', position='append', action='write')

                    write(file_unit, '(I0, 1X, F0.3, 1X, ES20.14, 1X, ES20.14)') &
                        mpi_rank, time_end - time_begin, wavenumber_min__, wavenumber_max__

                    close(file_unit)
                end if
            end do
        end subroutine write_processes_info
end module cross_sections