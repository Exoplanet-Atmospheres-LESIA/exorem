program caxs
    ! """
    ! Main of program Calculate Absorption X-Sections (CAXS).
    ! """
    use mpi_utils
    use interface, only: time_begin, time_end
    use cross_sections, only: calculate_species_absorption_cross_sections

    implicit none

    character(len=*), parameter :: caxs_version = '6.0.0'

    ! Initalization
    call cpu_time(time_begin)

    call get_caxs_command_arguments()

    call init_mpi()
    call intro_display()

    ! Main execution
    write(mpi_msg, '("Initialising...")')
    call mpi_print(mpi_msg, 0)
    call init_caxs()

    call calculate_species_absorption_cross_sections()

    ! Finalization
    call cpu_time(time_end)

    call outro_display()

    call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)
    call MPI_FINALIZE(mpi_ierr)

    contains
        subroutine init_caxs()
            ! """
            ! """
            use files, only: file_name_size
            use interface, only: path_data, path_lines, path_species_data, read_input_parameters, &
                read_species_broadening_blocks, read_species_data, read_species_electronic_states, &
                read_species_vibrational_modes, voigt_data_file
            use thermodynamics, only: allocate_thermodynamics_attributes
            use species, only: n_species, species_names, allocate_species_primary_attributes, &
                molar_masses, line_shape, rotational_partition_exponents, &
                electronic_states_degeneracies, electronic_states_wavenumbers, &
                species_broadenings, species_broadening_temperature_coefficients, &
                vibrational_modes_degeneracies, vibrational_modes_wavenumbers, &
                cutoffs, intensities_min, temperature_intensities_min, &
                n_broadenings_species, n_electronic_states_species, n_vibrational_modes_species, &
                allocate_species_secondary_attributes, lines_files

            implicit none

            character(len=file_name_size) :: &
                file_name

            integer :: &
                i

            call read_input_parameters()

            voigt_data_file = trim(path_data) // 'voigt.bin'

            ! Read species data
            do i = 1, n_species
                write(file_name, '(A)') trim(path_species_data) // trim(species_names(i)) // '.dat.nml'

                call read_species_data(&
                    trim(file_name), &
                    species_names(i), &
                    molar_masses(i), &
                    line_shape(i), &
                    rotational_partition_exponents(i), &
                    n_vibrational_modes_species(i), &
                    n_electronic_states_species(i), &
                    n_broadenings_species(i), &
                    cutoffs(i), &
                    intensities_min(i), &
                    temperature_intensities_min(i) &
                )
            end do

            call allocate_species_secondary_attributes()

            do i = 1, n_species
                write(lines_files(i), '(A)') trim(path_lines) // trim(species_names(i)) // '.geisa.txt'
                write(file_name, '(A)') trim(path_species_data) // trim(species_names(i)) // '.dat.nml'

                if(n_broadenings_species(i) > 0) then
                    call read_species_broadening_blocks(&
                        trim(file_name), &
                        n_broadenings_species(i), &
                        species_broadenings(i, :), &
                        species_broadening_temperature_coefficients(i, :) &
                    )
                end if

                if(n_electronic_states_species(i) > 0) then
                    call read_species_electronic_states(&
                        trim(file_name), &
                        n_electronic_states_species(i), &
                        electronic_states_wavenumbers(i, :), &
                        electronic_states_degeneracies(i, :) &
                    )
                end if

                if (n_vibrational_modes_species(i) > 0) then
                    call read_species_vibrational_modes(&
                        trim(file_name), &
                        n_vibrational_modes_species(i), &
                        vibrational_modes_wavenumbers(i, :), &
                        vibrational_modes_degeneracies(i, :) &
                    )
                end if
            end do
        end subroutine init_caxs

        subroutine init_mpi()
            ! """
            ! Initialise MPI and some useful parameters.
            ! """
            implicit none

            call MPI_INIT(mpi_ierr)
            call MPI_GET_LIBRARY_VERSION(mpi_version_, resultlen, mpi_ierr)
            call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_ierr)
            call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_nproc, mpi_ierr)
        end subroutine init_mpi

        subroutine intro_display()
            ! """
            ! Introduction display.
            ! """
            implicit none

            write(mpi_msg, '("CAXS ",A , " using ", A)') caxs_version, mpi_version_
            call mpi_print(NEW_LINE('A') // mpi_msg, 0)

            write(mpi_msg, '("Date: ", A24)') trim(ctime(time()))
            call mpi_print(mpi_msg, 0)

            write(mpi_msg, '("Number of processes:", I3)') mpi_nproc
            call mpi_print(trim(mpi_msg) // NEW_LINE('A'), 0)
        end subroutine intro_display

        subroutine outro_display()
            ! """
            ! Outroduction display.
            ! """
            implicit none

            write(mpi_msg, '("caxs: done !")')
            call mpi_print(NEW_LINE('A') // mpi_msg, 0)

            write(mpi_msg, '("Execution time (s):",ES15.6)') time_end - time_begin
            call mpi_print(trim(mpi_msg) // NEW_LINE('A'), 0)
        end subroutine outro_display

        subroutine get_caxs_command_arguments()
            ! """
            ! Retrieve the CAXS arguments that were passed on the command line when it was invoked.
            ! """
            implicit none

            character(len=255) :: arg
            integer :: i, j, n_args, n_opt_args

            j = 0
            n_args = 0
            n_opt_args = 0

            do i = 1, command_argument_count()
                call get_command_argument(i, arg)

                if(trim(arg) == '-v' .or. trim(arg) == '--version') then
                    write(*, '("CAXS ", A)') caxs_version

                    stop
                elseif(trim(arg) == '-h' .or. trim(arg) == '--help') then
                    call print_help()

                    stop
                else
                    j = j + 1
                end if

                if (len_trim(arg) == 0) then
                    if (j < n_args) then
                        write(*, '("Error: txt2h5 takes ", I0, " arguments but ", I0, " were given")') &
                            n_args, j
                        stop
                    end if

                    exit
                end if

                if(j > n_args) then
                    write(*, '("Error: ckc takes ", I0, " arguments but ", I0, " were given")') &
                        n_args + n_opt_args, j

                    stop
                end if
            end do
        end subroutine get_caxs_command_arguments

        subroutine print_help()
            implicit none

            character(len=*), parameter :: help_str = &
                "Usage: mpiexec -np <number_of_processes> caxs.exe [options]"&
                //new_line('')//"&
                &Example: mpiexec -np 4 caxs.exe"//new_line('')//"&
                &Options: "//new_line('')//"&
                &  --help         Display this information."//new_line('')//"&
                &  --version      Display software version information."//new_line('')//"&
                &"//new_line('')//"&
                &Input parameters must be in the file '../inputs/input_parameters.nml'."//new_line('')//"&
                &A working example of this file is available in the Gitlab of the software."//new_line('')//"&
                &Gitlab of the software:"//new_line('')//"&
                &<https://gitlab.obspm.fr/dblain/exorem>"

            write(*, '(A)') help_str
        end subroutine print_help
end program caxs