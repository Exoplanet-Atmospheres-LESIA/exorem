module mpi_utils
    ! """
    ! Contains the parameters needed for MPI and some useful MPI functions.
    ! """
    use mpi_f08

    implicit none

    character(len=512) :: &
        mpi_msg         ! string used to display messages in the terminal

    character(len=MPI_MAX_LIBRARY_VERSION_STRING) :: &
        mpi_version_    ! a string containing the Open MPI version

    integer, parameter :: &
        mpi_tag1 = 100  ! tag

    integer :: &
        mpi_ierr, &     ! run code
        mpi_nproc, &    ! number of processes in the run
        mpi_rank, &     ! rank of the process
        resultlen       ! length (in characters) of result returned in version (see mpi_version_)

    integer, dimension(MPI_STATUS_SIZE) :: &
        mpi_status1     ! status

    save

    contains
        function mpi_fftconvolve(signal, filter) result(convolved_signal)
            ! """
            ! Convolve a parallelised signal by a filter.
            ! Use FFT for convolution.
            !
            ! inputs:
            !   signal: array containing the signal
            !   filter; array containing the filter which will convolute the signal
            !
            ! output:
            !   convolved_signal: convoluted signal, same size than the signal
            !
            ! notes:
            !   The instrument function must be normalized.
            !   The floor(size(filter)/2) first and last points of the convoluted signal
            !   correspond to the convolution of the zero padding of the signal.
            ! """
            use math, only: fft, ifft, sgn

            implicit none

            doubleprecision, dimension(:), intent(in) :: filter, signal
            doubleprecision, dimension(size(signal)) :: convolved_signal

            integer :: &
                i, &            ! index
                size_c, &       ! size of complex arrays
                size_filter, &  ! size of filter array
                size_signal, &  ! size of signal array
                start_index     ! index where to start the returned array

            doubleprecision, dimension(size(signal)) :: &
                sign_signal  ! sign of signal array

            complex(kind=8), dimension(:), allocatable :: &
                c_filter, &  ! complex filter array
                c_signal     ! complex signal array

            complex(kind=8), dimension(size(signal)) :: &
                c_signal_centered  ! complex signal array without FFT shift

            ! Initialization
            size_signal = size(signal)
            size_filter = size(filter)
            start_index = floor(size_filter / 2D0) + 1

            ! Store sign
            do i=1,size_signal
                sign_signal(i) = sgn(signal(i))
            end do

            ! Adjust the size of the complex arrays to have enough zero padding and to be 2^n for fft
            size_c = 2**(int(log(dble(size_signal + size_filter)) / log(2D0)) + 1)

            allocate(c_filter(size_c), c_signal(size_c))

            ! Initialize complex arrays
            c_filter(:size_filter) = cmplx(filter, 0D0, kind=8)
            c_filter(size_filter + 1:) = cmplx(0D0, 0D0, kind=8)

            c_signal(:size_signal) = cmplx(signal, 0D0, kind=8)
            c_signal(size_signal + 1:) = cmplx(0D0, 0D0, kind=8)

            call send_c_signal()

            ! FFT convolution
            call fft(c_signal)
            call fft(c_filter)

            c_signal(:) = c_signal(:) * c_filter(:)

            call ifft(c_signal)

            ! Redistribution
            call recv_c_signal()

            ! Remove the convolution shift
            c_signal_centered(:) = c_signal(start_index:start_index + size_signal - 1)

            ! Back to the real domain
            convolved_signal(:) = sign_signal(:) * sqrt(dble(c_signal_centered(:))**2 + aimag(c_signal_centered(:))**2)

            ! Clean
            deallocate(c_filter, c_signal)

            return

            contains
                subroutine send_c_signal()
                    ! """
                    ! Send the relevant parts of the complex signal between processes.
                    ! """
                    implicit none

                    if(mpi_nproc > 1) then
                        if(mpi_rank > 0 .and. mpi_rank < mpi_nproc-1) then
                            call MPI_SENDRECV(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                        else
                            if(mpi_rank == 0) then
                                call MPI_RECV(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                            else
                                call MPI_SEND(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, mpi_ierr)
                                c_signal(size_signal + 1:size_signal + 1 + size_filter) = 0D0
                            end if
                        end if
                    end if
                end subroutine send_c_signal

                subroutine recv_c_signal()
                    ! """
                    ! Receive the relevants part of the complex signal between processes.
                    ! """
                    implicit none

                    if(mpi_nproc > 1) then
                        if(mpi_rank > 0 .and. mpi_rank < mpi_nproc - 1) then
                            call MPI_SENDRECV(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX,mpi_rank + 1, mpi_tag1, &
                                              c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                        else
                            if(mpi_rank == 0) then
                                call MPI_SEND(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, mpi_ierr)
                            else ! last process
                                call MPI_RECV(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                            end if
                        end if
                    end if
                end subroutine recv_c_signal
        end function mpi_fftconvolve


        function mpi_slide_fftconvolve(signal, sliding_filter, displacement) result(convolved_signal)
            ! """
            ! Convolve a parallelised signal by a sliding filter.
            ! Use FFT for convolution.
            !
            ! inputs:
            !   signal: array containing the signal
            !   sliding_filter: array containing the filter which will convolute the signal
            !   displacement: index displacement of signal for current process
            !
            ! output:
            !   convolved_signal: convoluted signal, same size than the signal
            !
            ! notes:
            !   The instrument function must be normalized.
            !   This function works but is *very* costly in term of computation time. Avoid if possible.
            !   The floor(size(filter)/2) first and last points of the convoluted signal
            !   correspond to the convolution of the zero padding of the signal.
            ! """
            use math, only: fft, ifft, sgn

            implicit none

            integer :: displacement
            doubleprecision, dimension(:), intent(in) :: signal
            doubleprecision, dimension(:, :), intent(in) :: sliding_filter
            doubleprecision, dimension(size(signal)) :: convolved_signal

            doubleprecision, parameter :: &
                tiniest = sqrt(tiny(0D0))

            integer :: &
                i, &            ! index
                j, &            ! index
                size_c, &       ! size of complex arrays
                size_filter, &  ! size of filter array
                size_filter2, &  ! size of filter array
                size_signal, &  ! size of signal array
                start_index     ! index where to start the returned array

            doubleprecision, dimension(size(signal)) :: &
                sign_signal  ! sign of signal array

            complex(kind=8), dimension(:), allocatable :: &
                c_signal, &  ! complex signal array
                c_signal_j_ifft, &  ! complex signal array
                c_signal_j   ! complex signal array at sample j

            complex(kind=8), dimension(:, :), allocatable :: &
                c_filter    ! complex filter array

            complex(kind=8), dimension(size(signal)) :: &
                c_signal_centered  ! complex signal array without FFT shift

            ! Initialization
            size_signal = size(signal)
            size_filter = size(sliding_filter, 1)
            size_filter2 = size(sliding_filter, 2)
            start_index = floor(size_filter / 2D0) + 1

            ! Store sign
            do i = 1, size_signal
                sign_signal(i) = sgn(signal(i))
            end do

            ! Adjust the size of the complex arrays to have enough zero padding and to be 2^n for fft
            size_c = 2**(int(log(dble(size_signal + size_filter)) / log(2D0)) + 1)

            allocate(&
                c_filter(size_c, size_c), &
                c_signal(size_c), &
                c_signal_j(size_c), &
                c_signal_j_ifft(size_c)&
            )

            ! Initialize complex arrays
            do i = 1, size_c
                j = min(max(displacement - start_index + i, 1), size_filter2)
                c_filter(:size_filter, i) = cmplx(sliding_filter(:, j), 0D0, kind=8)
                c_filter(size_filter + 1:, i) = cmplx(0D0, 0D0, kind=8)
            end do

            c_signal(:size_signal) = cmplx(signal, 0D0, kind=8)
            c_signal(size_signal + 1:) = cmplx(0D0, 0D0, kind=8)

            call send_c_signal()

            ! FFT convolution
            call fft(c_signal)

            ! Step 2: j = [2; size_signal]
            do i = 1, size_c
                j = i

                call fft(c_filter(:, j))

                c_signal_j(:) = c_signal(:) * c_filter(:, j)

                call ifft(c_signal_j)

                c_signal_j_ifft(i) = c_signal_j(i)
            end do

            ! Final
            c_signal(:) = c_signal_j_ifft(:)

            ! Check result to avoid future denormal exception
            do i = 1, size_c
                if (abs(dble(c_signal(i))) <= tiniest) then
                    c_signal(i) =  cmplx(0D0, aimag(c_signal(i)), kind=8)
                end if

                if (abs(aimag(c_signal(i))) <= tiniest) then
                    c_signal(i) =  cmplx(dble(c_signal(i)), 0D0, kind=8)
                end if
            end do

            ! Redistribution
            call recv_c_signal()

            ! Remove the convolution shift
            c_signal_centered(:) = c_signal(start_index:start_index + size_signal - 1)

            ! Back to the real domain
            convolved_signal(:) = sign_signal(:) * sqrt(dble(c_signal_centered(:))**2 + aimag(c_signal_centered(:))**2)

            ! Clean
            deallocate(c_filter, c_signal)

            return

            contains
                subroutine send_c_signal()
                    ! """
                    ! Send the relevant parts of the complex signal between processes.
                    ! """
                    implicit none

                    if(mpi_nproc > 1) then
                        if(mpi_rank > 0 .and. mpi_rank < mpi_nproc-1) then
                            call MPI_SENDRECV(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                        else
                            if(mpi_rank == 0) then
                                call MPI_RECV(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                            else
                                call MPI_SEND(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, mpi_ierr)
                                c_signal(size_signal + 1:size_signal + 1 + size_filter) = 0D0
                            end if
                        end if
                    end if
                end subroutine send_c_signal

                subroutine recv_c_signal()
                    ! """
                    ! Receive the relevants part of the complex signal between processes.
                    ! """
                    implicit none

                    if(mpi_nproc > 1) then
                        if(mpi_rank > 0 .and. mpi_rank < mpi_nproc - 1) then
                            call MPI_SENDRECV(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX,mpi_rank + 1, mpi_tag1, &
                                              c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                        else
                            if(mpi_rank == 0) then
                                call MPI_SEND(c_signal(size_signal + 1:size_signal + size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank + 1, mpi_tag1, &
                                              MPI_COMM_WORLD, mpi_ierr)
                            else ! last process
                                call MPI_RECV(c_signal(:size_filter), size_filter, &
                                              MPI_DOUBLE_COMPLEX, mpi_rank - 1, mpi_tag1, &
                                              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpi_ierr)
                            end if
                        end if
                    end if
                end subroutine recv_c_signal
        end function mpi_slide_fftconvolve


        function mpi_slide_convolve(signal__, filter, displacement) result(convolved_signal)
            ! """
            ! Slide convolve the signal by a sliding filter using classical convolution.
            ! TODO [low] work on this (does not currently work)
            !
            ! inputs:
            !   signal__: the parallelised signal array
            !   filter: the filter 2D-array
            !   displacement: index displacement of signal for current process
            !
            ! output:
            !   convolved_signal: signal convolved by filter
            !
            ! source: https://fortrandev.wordpress.com/2013/04/01/fortran-convolution-algorithm/
            ! """
            implicit none

            integer, intent(in) :: displacement
            doubleprecision, dimension(:), intent(in) :: signal__
            doubleprecision, dimension(:, :), intent(in) :: filter
            doubleprecision, dimension(:), allocatable :: convolved_signal, convolution

            integer :: &
                i, &  ! index
                i_max, &  ! max index of convolution
                i_min, &  ! min index of convolution
                j, &  ! index
                j__, & ! paralellised index
                k, &  ! index
                size_filter, &  ! size of the filter
                size_signal, &  ! size of the signal
                size_signal__, &  ! size of the signal
                start_index, &    ! start index of the result
                stop_index    ! start index of the result

            ! Initilization
            size_signal__ = size(signal__)
            size_signal = size(filter, 2)
            size_filter = size(filter, 1)

            i_min = displacement + 1

            start_index = floor(size_filter / 2D0) + i_min
            stop_index = start_index + size_signal__ - 1

            i_max = displacement + size_signal__ + start_index

            allocate(convolved_signal(size_signal__), convolution(size_signal + size_filter))

            convolution(:) = 0D0

            ! Last part
            do i = max(size_signal, i_min), min(size_signal + size_filter, i_max)
                j = i
                j__ = size_signal - displacement

                do k = 1, size_filter
                    convolution(i) = convolution(i) + signal__(j__) * filter(k, j)
                    j = j - 1
                    j__ = max(j__ - 1, 1)
                end do
            end do

            ! Middle part
            do i = max(size_filter, i_min), min(size_signal, i_max)
                j = i
                j__ = i - displacement

                do k = 1, size_filter
                    convolution(i) = convolution(i) + signal__(j__) * filter(k, j)
                    j = j - 1
                    j__ = max(j__ - 1, 1)
                end do
            end do

            ! First part
            do i = i_min, min(size_filter, i_max)
                j = i
                j__ = i - displacement
                k = 1

                do while (j > 0)
                    convolution(i) = convolution(i) + signal__(j__) * filter(k, j)
                    j = j - 1
                    j__ = max(j__ - 1, 1)
                    k = k + 1
                end do
            end do

            convolved_signal(:) = convolution(start_index:stop_index)
        end function mpi_slide_convolve


        subroutine mpi_allgatherv1D(sendbuf, recvbuf)
            ! """
            ! MPI_ALLGATHERV for double precision arrays.
            !
            ! input:
            !   sendbuf: send buffer (double precision array)
            !
            ! output:
            !   recvbuf: receive buffer (double precision array)
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            doubleprecision, dimension(:), intent(in) :: sendbuf
            doubleprecision, dimension(:), intent(out) :: recvbuf

            integer :: &
                displ, &      ! index displacement
                size_sendbuf  ! size of sending buffer

            integer, dimension(1) :: &
                shape_recvbuf, &  ! shape of receiving buffer
                shape_sendbuf     ! shape of sending buffer

            integer, dimension(mpi_nproc) :: &
                displs, &  ! index displacements of each process
                sizes      ! sizes of sending buffer of each process

            shape_sendbuf = shape(sendbuf)
            shape_recvbuf = shape(recvbuf)
            size_sendbuf = size(sendbuf(:))

            call MPI_ALLGATHER(size_sendbuf, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if(sum(sizes) /= size(recvbuf(:))) then
                write(mpi_msg, '("mpi_allgatherv1D: sum(size(sendbuf)) /= size(recvbuf) (", I10, ",", I10, ")")') &
                      sum(sizes), size(recvbuf(:))
                call mpi_print_error(mpi_msg, mpi_rank)
            end if

            if(mpi_rank > 0) then
                displ = sum(sizes(1:mpi_rank))
            else
                displ = 0
            end if

            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            call MPI_ALLGATHERV(sendbuf(:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                recvbuf(:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                MPI_COMM_WORLD, mpi_ierr)
        end subroutine mpi_allgatherv1D


        subroutine mpi_allgatherv2D(sendbuf, recvbuf)
            ! """
            ! MPI_ALLGATHERV for 2D matrix.
            !
            ! input:
            !   sendbuf: send buffer (double precision 2D matrix)
            !
            ! output:
            !   recvbuf: receive buffer (double precision 2D matrix)
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            doubleprecision, dimension(:,:), intent(in) :: sendbuf
            doubleprecision, dimension(:,:), intent(out) :: recvbuf

            integer :: &
                displ, &      ! index displacement
                np, &         ! number of line in sending buffer
                size_sendbuf  ! size of sending buffer

            integer, dimension(2) :: &
                shape_recvbuf, &  ! shape of receiving buffer
                shape_sendbuf     ! shape of sending buffer

            integer, dimension(mpi_nproc) :: &
                displs, &  ! index displacements of each process
                sizes      ! sizes of sending buffer of each process

            doubleprecision, dimension(:,:), allocatable :: &
                reshaped_sendbuf  ! reshaped sending buffer for direct gathering

            shape_sendbuf = shape(sendbuf)
            shape_recvbuf = shape(recvbuf)
            size_sendbuf = size(sendbuf(:,:))

            call MPI_ALLGATHER(size_sendbuf, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if(sum(sizes) /= size(recvbuf(:,:))) then
                write(mpi_msg, '("mpi_allgatherv2D: sum(size(sendbuf)) /= size(recvbuf) (", I10, ",", I10, ")")') &
                      sum(sizes), size(recvbuf(:,:))
                call mpi_print_error(mpi_msg, mpi_rank)
            end if

            if(mpi_rank > 0) then
                displ = sum(sizes(1:mpi_rank))
            else
                displ = 0
            end if

            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if (shape_sendbuf(1) < shape_recvbuf(1)) then
                np = shape_sendbuf(1)

                allocate(reshaped_sendbuf(np, shape_recvbuf(2)))
                reshaped_sendbuf(:,:) = reshape(sendbuf(:,:),(/shape_recvbuf(2), np/), order=(/2, 1/))

                call MPI_ALLGATHERV(reshaped_sendbuf(:,:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                    recvbuf(:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                    MPI_COMM_WORLD, mpi_ierr)

                recvbuf(:,:) = reshape(recvbuf(:,:),shape_recvbuf(:),order=(/2,1/))

                deallocate(reshaped_sendbuf)

                return
            end if

            if(shape_sendbuf(2) <= shape_recvbuf(2)) then
                call MPI_ALLGATHERV(sendbuf(:,:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                    recvbuf(:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                    MPI_COMM_WORLD, mpi_ierr)
                return
            end if

            call mpi_print_error('mpi_allgatherv2D: wrong shape', mpi_rank)
        end subroutine mpi_allgatherv2D


        subroutine mpi_allgatherv3D(sendbuf, recvbuf)
            ! """
            ! MPI_ALLGATHERV for double precision 3D matrix.
            !
            ! input:
            !   sendbuf: send buffer (double precision 3D matrix)
            !
            ! output:
            !   recvbuf: receive buffer (double precision 3D matrix)
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            doubleprecision, dimension(:,:,:), intent(in) :: sendbuf
            doubleprecision, dimension(:,:,:), intent(out) :: recvbuf

            integer :: size_sendbuf, displ, np
            integer, dimension(3) :: shape_sendbuf, shape_recvbuf
            integer, dimension(mpi_nproc) :: sizes, displs
            doubleprecision, dimension(:,:,:), allocatable :: reshaped_sendbuf

            shape_sendbuf = shape(sendbuf)
            shape_recvbuf = shape(recvbuf)
            size_sendbuf = size(sendbuf(:,:,:))

            call MPI_ALLGATHER(size_sendbuf, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if(sum(sizes) /= size(recvbuf(:,:,:))) then
                ! TODO [low] investigate why this error occurs when the synthetic spectrum sample number is a multiple of the observed spectrum sample number
                ! (i.e. in NRT/Example_scripts/040_retrieve.py, after executing all the other example scripts, the
                ! error occurs when wavenumber_step of spec_retrieved is set to 0.5, but not when set to 0.5001)
                write(mpi_msg, '("mpi_allgatherv3D: sum(size(sendbuf)) /= size(recvbuf) (", I10, ",", I10, ")")') &
                      sum(sizes), size(recvbuf(:,:,:))
                call mpi_print_error(mpi_msg, mpi_rank)
            end if

            if(mpi_rank > 0) then
                displ = sum(sizes(1:mpi_rank))
            else
                displ = 0
            end if

            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if (shape_sendbuf(1) < shape_recvbuf(1)) then
                np = shape_sendbuf(1)

                allocate(reshaped_sendbuf(shape_recvbuf(3),shape_recvbuf(2),np))
                reshaped_sendbuf(:,:,:) = reshape(sendbuf(:,:,:), (/shape_recvbuf(3), shape_recvbuf(2), np/), &
                                                  order=(/3, 2, 1/))

                call MPI_ALLGATHERV(reshaped_sendbuf(:,:,:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                    recvbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                    MPI_COMM_WORLD, mpi_ierr)
                recvbuf(:,:,:) = reshape(recvbuf(:,:,:),shape_recvbuf(:),order=(/3,2,1/))

                deallocate(reshaped_sendbuf)
            else if(shape_sendbuf(2) < shape_recvbuf(2)) then
                np = shape_sendbuf(2)

                allocate(reshaped_sendbuf(shape_recvbuf(1),np,shape_recvbuf(3)))
                reshaped_sendbuf(:,:,:) = reshape(sendbuf(:,:,:), (/shape_recvbuf(1), shape_recvbuf(3),np/), &
                                                  order=(/1,3,2/))

                call MPI_ALLGATHERV(reshaped_sendbuf(:,:,:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                    recvbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                    MPI_COMM_WORLD, mpi_ierr)

                recvbuf(:,:,:) = reshape(recvbuf(:,:,:),shape_recvbuf(:),order=(/1,3,2/))

                deallocate(reshaped_sendbuf)
            else if(shape_sendbuf(3) <= shape_recvbuf(3)) then
                call MPI_ALLGATHERV(sendbuf(:,:,:), size_sendbuf, MPI_DOUBLE_PRECISION, &
                                    recvbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                    MPI_COMM_WORLD, mpi_ierr)
            else
                call mpi_print_error('mpi_allgatherv3D: wrong shape', mpi_rank)
            end if

        end subroutine mpi_allgatherv3D


        subroutine mpi_fair_share(n0, n, displ)
            ! """
            ! Redistributes equitably the size and the indexes of an array to all tasks in a group.
            ! Useful for the paralellization of a loop over an array and for all *v mpi functions, like MPI_SCATTERV.
            !
            ! input:
            !   n0: original size
            !
            ! outputs:
            !   n: new size for a process can be used to build the sendcounts array for a *v mpi function
            !   displ: displacement relative to n0 for a process can be used to build the displs array for a *v mpi
            !          function
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            integer, intent(in) :: n0
            integer, intent(out) :: displ, n

            if (mpi_nproc > n0 .and. mpi_rank == 0) then
                print '("WARNING: number of processors (",I6,") > number of elements (",I6,")")', mpi_nproc, n0
                print '(1X,"> prepare for unforeseen consequences...")'
            end if

            if (mod(n0, mpi_nproc) == 0) then
                n = n0  /mpi_nproc
                displ = n * mpi_rank
            else
                if (mpi_rank < mod(n0, mpi_nproc)) then
                    n = floor(real(n0 / mpi_nproc)) + 1
                    displ = n * mpi_rank
                else
                    n = floor(real(n0 / mpi_nproc))
                    if (mpi_rank == mod(n0, mpi_nproc)) then
                        displ = (n + 1) * mpi_rank
                    else
                        displ = (n + 1) * mpi_rank + mod(n0, mpi_nproc) - mpi_rank
                    end if
                end if
            end if

            return

        end subroutine mpi_fair_share


        subroutine mpi_fair_sharev(recvcount, sendcount, sendcounts, displ, displs)
            ! """
            ! Redistributes equitably the size and the indexes of an array for all tasks in a group, then build the
            ! input arrays needed for any *v MPI function.
            ! Useful for the paralellization of a loop over an array and for all *v mpi functions, like MPI_SCATTERV.
            !
            ! input:
            !   recvcount: number of elements in receive buffer (integer)
            !
            ! outputs:
            !   sendcount: number of elements in send buffer (integer)
            !   sendcounts: integer array (of length group size) specifying the number of elements to send to each
            !               processor
            !   displ: displacement relative to sendbuf from which to take the outgoing data to the current process
            !   displs: integer array (of length group size). Entry i specifies the displacement relative to sendbuf
            !           from which to take the outgoing data to process i
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            integer, intent(in) :: recvcount
            integer, intent(out) :: sendcount, displ
            integer, intent(out), dimension(mpi_nproc) :: sendcounts, displs

            call mpi_fair_share(recvcount, sendcount, displ)
            call MPI_ALLGATHER(sendcount, 1, MPI_INTEGER, sendcounts, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)
            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            return

        end subroutine mpi_fair_sharev


        subroutine mpi_matinv(A, A_inv, n0)
            ! """
            ! Inverse a 2D matrix of dimension (n0, n0) using MPI.
            !
            ! inputs:
            !   A(n0, n0): matrix to inverse
            !   n0: dimension of the matrix
            !
            ! output:
            !   A_inv(n0, n0): inverse of matrix A
            !
            ! notes:
            !   Method: Parallel gaussian ellimination
            !   Based on: CSE 633 Parallel Algorithms (Spring 2014) by Aravindhan Thanigachalam (athaniga@buffalo.edu)
            !   Dependent of subroutine mpi_fair_sharv, which redistributes equitably indexes to all tasks in a group.
            ! """
            implicit none

            integer, intent(in) :: n0
            doubleprecision, dimension(n0, n0), intent(in) :: A
            doubleprecision, dimension(n0, n0), intent(inout) :: A_inv

            integer :: &
                i, &      ! index
                i__, &    ! column of matrix A__ corresponding to column i of matrix A
                j, &      ! column index
                j_min, &  ! column of matrix A corresponding to the first column of matrix A__
                j_max, &  ! column of matrix A corresponding to the last column of matrix A__
                jz, &     ! zeroing column of matrix A
                jz__, &   ! zeroing column of matrix A__
                ncol, &   ! number of columns in matrix A__
                r, &      ! row where A(r,i) /= 0
                root, &   ! rank of the process where the column i is
                size_A__  ! size of matrix A__

            integer, dimension(mpi_nproc) :: &  ! mpi_fair_sharev utils
                ncols, &     ! number of columns of matrix A__ of each processes
                sizes, &     ! size_A__ of all the processes
                displs_j, &  ! index displacement
                displs_j0    ! index displacement

            doubleprecision :: &
                scale  ! Gauss-Jordan elimination step 2 scale

            doubleprecision, dimension(n0) :: &
                factor  ! Gauss-Jordan elimination step 2 factor row

            doubleprecision, dimension(:), allocatable :: &
                row  ! row storage for row swapping

            doubleprecision, dimension(n0, n0) :: &
                Id  ! identity matrix

            doubleprecision, dimension(:, :), allocatable :: &
                A__, &   ! slice of matrix A in a process
                A_inv__  ! slice of matrix A_inv in a process

            factor(:) = 0D0

            ! Build identity matrix
            Id(:, :) = 0D0
            do i = 1, n0
                Id(i, i) = 1D0
            end do

            ! Column wise distribution
            call mpi_fair_sharev(n0, ncol, ncols, j_min, displs_j0)
            j_min = j_min + 1 ! fortran index format
            j_max = j_min + ncol - 1

            allocate(A__(n0, ncol), A_inv__(n0, ncol), row(ncol))
            A__(:, :) = A(:, j_min:j_max)
            A_inv__(:, :) = Id(:, j_min:j_max)
            row(:) = 0D0

            ! Gaussian elimination phase
            i__ = 1
            root = 0

            do i = 1, n0
                ! Step 1: swap row i with the nearest subsequent row r such that after swapping A(i,i) /= 0
                ! Step 1a: find the rank (root) of the process where the column i is
                if (i > displs_j0(root + 1) + ncols(root + 1)) then
                    root = root + 1
                end if

                ! Step 1b: find the nearest subsequent row r where A(r,i) /= 0
                if (mpi_rank == root) then
                    r = i
                    i__ = i - sum(ncols(1:root))
                    do while (abs(A__(r, i__)) < tiny(0.))
                        r = r + 1
                        if (r > n0) then
                            if (mpi_rank == 0) print '("mpi_matinv: Inverse does not exist")'
                            return
                        end if
                    end do
                end if

                call MPI_BCAST(r, 1, MPI_INTEGER, root, MPI_COMM_WORLD, mpi_ierr)

                ! Step 1c: swap row i with row r
                if (r > i) then
                    row(:) = A__(i, :)
                    A__(i, :) = A__(r, :)
                    A__(r, :) = row(:)

                    row(:) = A_inv__(i, :)
                    A_inv__(i, :) = A_inv__(r, :)
                    A_inv__(r, :) = row(:)
                end if

                ! Step 2: divide row i by scale = A(i,i)
                scale = A__(i, i__)
                call MPI_BCAST(scale, 1, MPI_DOUBLE_PRECISION, root, MPI_COMM_WORLD, mpi_ierr)

                do j = 1, ncol
                    A__(i, j) = A__(i, j) / scale
                    A_inv__(i, j) = A_inv__(i, j) / scale
                end do

                if (mpi_rank == root) then
                    factor(:) = A__(:, i__)
                end if

                call MPI_BCAST(factor(:), n0, MPI_DOUBLE_PRECISION, root, MPI_COMM_WORLD, mpi_ierr)

                ! Step 3: change all rows below row i to take the previous modifications into account
                if (i < n0) then
                    do r = i + 1, n0
                        do j = 1, ncol
                            A__(r, j) = A__(r, j) - factor(r) * A__(i, j)
                            A_inv__(r, j) = A_inv__(r, j) - factor(r) * A_inv__(i, j)
                        end do
                    end do
                end if
            end do

            ! Back substitution phase
            root = mpi_nproc - 1
            jz__ = 1

            do jz = n0, 2, -1
                ! Step 1: find the rank of the process where column jz (zeroing column) is
                if (jz < displs_j0(root + 1) + 1) then
                    root = root - 1
                end if

                if (mpi_rank == root) then
                    jz__ = jz - sum(ncols(1:root))
                    factor(:) = A__(:, jz__)
                end if
                call MPI_BCAST(factor(:), n0, MPI_DOUBLE_PRECISION, root, MPI_COMM_WORLD, mpi_ierr)

                ! Step 2: transform matrix A_inv into A^-1 (doing the same transformation on A gives the Id matrix)
                do i = jz - 1, 1, -1
                    do j = 1, ncol
                        A_inv__(i, j) = A_inv__(i, j) - factor(i) * A_inv__(jz, j)
                    end do
                end do
            end do

            ! Gather inversed matrix
            size_A__ = size(A__(:, :))
            call MPI_ALLGATHER(size_A__, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            j = sum(sizes(1:mpi_rank))
            call MPI_ALLGATHER(j, 1, MPI_INTEGER, displs_j, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            call MPI_ALLGATHERV(A_inv__(:, :), size_A__, MPI_DOUBLE_PRECISION, &
                                A_inv(:, :), sizes(:), displs_j(:), MPI_DOUBLE_PRECISION, &
                                MPI_COMM_WORLD, mpi_ierr)

            deallocate(A__, A_inv__, row)

        end subroutine mpi_matinv


        subroutine mpi_scatterv1D(sendbuf, recvbuf, root)
            ! """
            ! MPI_SCATTERV for double precision arrays.
            !
            ! inputs:
            !   sendbuf: send buffer (double precision array)
            !   root: rank of sending process
            !
            ! output:
            !   recvbuf: receive buffer (double precision array)
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            integer, intent(in) :: root
            doubleprecision, dimension(:), intent(in) :: sendbuf
            doubleprecision, dimension(:), intent(out) :: recvbuf

            integer :: &
                displ, &      ! index displacement
                size_recvbuf  ! size of receiving buffer

            integer, dimension(1) :: &
                shape_recvbuf, &  ! shape of receiving buffer
                shape_sendbuf     ! shape of sending buffer

            integer, dimension(mpi_nproc) :: &
                displs, &  ! index displacements of each process
                sizes      ! sizes of sending buffer of each process

            shape_sendbuf = shape(sendbuf)
            shape_recvbuf = shape(recvbuf)
            size_recvbuf = size(recvbuf(:))

            call MPI_ALLGATHER(size_recvbuf, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if(sum(sizes) /= size(sendbuf(:))) then
                write(mpi_msg, '("mpi_scatterv1D: sum(size(recvbuf)) /= size(sendbuf) (", I10, ",", I10, ")")') &
                      sum(sizes), size(recvbuf(:))
                call mpi_print_error(mpi_msg, mpi_rank)
            end if

            if(mpi_rank > 0) then
                displ = sum(sizes(1:mpi_rank))
            else
                displ = 0
            end if

            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            call MPI_SCATTERV(sendbuf(:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                              recvbuf(:), size_recvbuf, MPI_DOUBLE_PRECISION, &
                              root, MPI_COMM_WORLD, mpi_ierr)
        end subroutine mpi_scatterv1D


        subroutine mpi_scatterv3D(sendbuf, recvbuf, root)
            ! """
            ! MPI_SCATTERV for double precision 3D matrix.
            !
            ! inputs:
            !   sendbuf: send buffer (double precision array)
            !   root: rank of sending process
            !
            ! output:
            !   recvbuf: receive buffer (double precision array)
            !
            ! notes:
            !   mpi_rank and mpi_nproc must have been initialized
            ! """
            implicit none

            integer, intent(in) :: root
            doubleprecision, dimension(:,:,:), intent(in) :: sendbuf
            doubleprecision, dimension(:,:,:), intent(out) :: recvbuf

            integer :: &
                displ, &      ! index displacement
                np, &
                size_recvbuf  ! size of receiving buffer

            integer, dimension(3) :: &
                shape_recvbuf, &  ! shape of receiving buffer
                shape_sendbuf     ! shape of sending buffer

            integer, dimension(mpi_nproc) :: &
                displs, &  ! index displacements of each process
                sizes      ! sizes of sending buffer of each process

            doubleprecision, dimension(:,:,:), allocatable :: &
                reshaped_sendbuf

            shape_sendbuf = shape(sendbuf)
            shape_recvbuf = shape(recvbuf)
            size_recvbuf = size(recvbuf(:,:,:))

            call MPI_ALLGATHER(size_recvbuf, 1, MPI_INTEGER, sizes, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if(sum(sizes) /= size(sendbuf(:,:,:))) then
                write(mpi_msg, '("mpi_scatterv3D: sum(size(recvbuf)) /= size(sendbuf) (", I10, ",", I10, ")")') &
                      sum(sizes), size(recvbuf(:,:,:))
                call mpi_print_error(mpi_msg, mpi_rank)
            end if

            if(mpi_rank > 0) then
                displ = sum(sizes(1:mpi_rank))
            else
                displ = 0
            end if

            call MPI_ALLGATHER(displ, 1, MPI_INTEGER, displs, 1, MPI_INTEGER, MPI_COMM_WORLD, mpi_ierr)

            if (shape_recvbuf(1) < shape_sendbuf(1)) then
                np = shape_sendbuf(1)

                allocate(reshaped_sendbuf(shape_sendbuf(3), shape_sendbuf(2), np))
                reshaped_sendbuf(:,:,:) = reshape(sendbuf(:,:,:), (/shape_sendbuf(3), shape_sendbuf(2), np/), &
                                                  order=(/3, 2, 1/))

                call MPI_SCATTERV(reshaped_sendbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                  recvbuf(:,:,:), size_recvbuf, MPI_DOUBLE_PRECISION, &
                                  root, MPI_COMM_WORLD, mpi_ierr)

                recvbuf(:,:,:) = reshape(recvbuf(:,:,:),shape_recvbuf(:),order=(/3, 2, 1/))

                deallocate(reshaped_sendbuf)
            else if(shape_recvbuf(2) < shape_sendbuf(2)) then
                np = shape_sendbuf(2)

                allocate(reshaped_sendbuf(shape_sendbuf(1), np, shape_sendbuf(3)))
                reshaped_sendbuf(:,:,:) = reshape(sendbuf(:,:,:), (/shape_sendbuf(1), shape_sendbuf(3),np/), &
                                                  order=(/1, 3, 2/))

                call MPI_SCATTERV(reshaped_sendbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                  recvbuf(:,:,:), size_recvbuf, MPI_DOUBLE_PRECISION, &
                                  root, MPI_COMM_WORLD, mpi_ierr)

                recvbuf(:,:,:) = reshape(recvbuf(:,:,:),shape_recvbuf(:),order=(/1, 3, 2/))

                deallocate(reshaped_sendbuf)
            else if(shape_recvbuf(3) <= shape_sendbuf(3)) then
                call MPI_SCATTERV(sendbuf(:,:,:), sizes(:), displs(:), MPI_DOUBLE_PRECISION, &
                                  recvbuf(:,:,:), size_recvbuf, MPI_DOUBLE_PRECISION, &
                                  root, MPI_COMM_WORLD, mpi_ierr)
            else
                call mpi_print_error('mpi_allgatherv3D: wrong shape', mpi_rank)
            end if
        end subroutine mpi_scatterv3D


        subroutine mpi_print(string, rank)
            ! """
            ! Print a text in the process with rank 0.
            ! Variables mpi_rank and mpi_nproc must have been initialized.
            !
            ! input:
            !   string: (string) text to print
            !   rank: process where to print the message
            ! """
            implicit none

            character(*), intent(in) :: string
            integer, intent(in) :: rank

            if (mpi_rank == rank) then
                print '(A)', trim(string)
            end if
        end subroutine mpi_print


        subroutine mpi_print_error(string, rank)
            ! """
            ! Print an error in the process with rank 0, then stop the program.
            ! Variables mpi_rank and mpi_nproc must have been initialized.
            !
            ! input:
            !   string: (string) error message to print
            !   rank: process where to print the message
            ! """
            implicit none

            character(*), intent(in) :: string
            integer, intent(in) :: rank

            call mpi_print_stderr('Error: ' // string, rank)
            stop
        end subroutine mpi_print_error


        subroutine mpi_print_info(string, rank)
            ! """
            ! Print an information in the process with rank 0.
            ! Variables mpi_rank and mpi_nproc must have been initialized.
            !
            ! input:
            !   string: (string) information message to print
            !   rank: process where to print the message
            ! """
            implicit none

            character(*), intent(in) :: string
            integer, intent(in) :: rank

            call mpi_print_stderr('Info: ' // string, rank)
        end subroutine mpi_print_info


        subroutine mpi_print_stderr(string, rank)
            ! """
            ! Print a text in the process with rank 0 in standard error output.
            ! Variables mpi_rank and mpi_nproc must have been initialized.
            !
            ! input:
            !   string: (string) text to print
            !   rank: process where to print the message
            ! """
            implicit none

            character(*), intent(in) :: string
            integer, intent(in) :: rank

            if (mpi_rank == rank) then
                write(0, '(A)') trim(string)
            end if

            call MPI_BARRIER(MPI_COMM_WORLD, mpi_ierr)  ! clean output
        end subroutine mpi_print_stderr


        subroutine mpi_print_warning(string, rank)
            ! """
            ! Print a warning in the process with rank 0.
            ! Variables mpi_rank and mpi_nproc must have been initialized.
            !
            ! input:
            !   string: (character) warning message to print
            !   rank: process where to print the message
            ! """
            implicit none

            character(*), intent(in) :: string
            integer, intent(in) :: rank

            call mpi_print_stderr('Warning: ' // string, rank)
        end subroutine mpi_print_warning
end module mpi_utils
