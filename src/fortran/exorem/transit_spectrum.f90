module transit_spectrum
    implicit none

    contains
        subroutine calculate_transit_spectrum(tau, tau_rayleigh, ng, weights_k, spectral_radius, d_spectral_radius, &
            calculate_contribution)
            ! """
            ! """
            use atmosphere, only: n_levels, n_layers, z
            use spectrometrics, only: n_wavenumbers
            use target, only: target_radius

            implicit none

            logical, intent(in) :: calculate_contribution
            integer, intent(in) :: ng
            doubleprecision, intent(in) :: &
                tau(n_levels, n_wavenumbers, ng),  tau_rayleigh(n_levels, n_wavenumbers), weights_k(ng)
            doubleprecision, intent(out) :: &
                spectral_radius(n_wavenumbers), d_spectral_radius(n_levels, n_wavenumbers)

            integer :: i, ig, j, l, k
            doubleprecision :: sec(n_layers, n_layers), transmittance_l(n_levels), tau_ig, z02, z12, z22, &
                alpha_j, area_l(n_levels), transmittance_j, transmittance_l2, trans_ig(n_levels, ng)

            write(*, '("Calculating transit spectrum...")')

            ! Calculation of the airmasses in each layer (j) for each line-of-sight (l)
            sec(:, :) = 0d0
            transmittance_l(:) = 1d0
            d_spectral_radius(:, :) = 0d0
            trans_ig(:, :) = 0d0

            do l = 1, n_layers
                z02 = (target_radius + z(l)) ** 2

                if(l == 1) then
                    area_l(l) = (target_radius + z(l)) * (z(2) - z(1))
                else
                    area_l(l) = (target_radius + z(l)) * (z(l + 1) - z(l - 1))
                endif

                do j = l, n_layers
                    z12 = (target_radius + z(j)) ** 2
                    z22 = (target_radius + z(j + 1)) ** 2
                    sec(j, l) = (sqrt(z22 - z02) - sqrt(z12 - z02)) / (z(j + 1) - z(j))
                end do
            end do

            ! Calculation of the radius as a function of wavenumber (i)
            do i = 1, n_wavenumbers
                ! Spectral radius
                spectral_radius(i) = (target_radius + z(1)) ** 2

                do l = 1, n_layers
                    transmittance_l(l) = 0d0

                    do ig = 1, ng
                        tau_ig = 0d0

                        do j = l, n_layers
                            alpha_j = &
                                (tau(j, i, ig) - tau(j + 1, i, ig) + &
                                 tau_rayleigh(j, i) - tau_rayleigh(j + 1, i) &
                                )
                            tau_ig = tau_ig + alpha_j * sec(j, l)
                        enddo

                        trans_ig(l, ig) = exp(-2d0 * tau_ig)
                        transmittance_l(l) = transmittance_l(l) + exp(-2d0 * tau_ig) * weights_k(ig)  ! transmittance of line-of-sight crossing down to layer l
                    enddo

                    if (transmittance_l(l) < tiny(0d0)) then
                        transmittance_l(l) = 0d0
                    else if (transmittance_l(l) > 1d0) then
                        transmittance_l(l) = 1d0
                    end if

                    spectral_radius(i) = spectral_radius(i) + area_l(l) * (1d0 - transmittance_l(l))
                enddo

                ! Contribution
                if(calculate_contribution) then
                    do j = 1, n_layers
                        do l = 1, j
                            transmittance_j = 0d0

                            do ig = 1, ng
                                tau_ig = 0d0

                                do k = l, n_layers
                                    alpha_j = &
                                        (tau(k, i, ig) - tau(k + 1, i, ig) + &
                                         tau_rayleigh(k, i) - tau_rayleigh(k + 1, i) &
                                        )
                                    tau_ig = tau_ig + alpha_j * sec(k, l)
                                end do

                                alpha_j = &
                                        (tau(j, i, ig) - tau(j + 1, i, ig) + &
                                         tau_rayleigh(j, i) - tau_rayleigh(j + 1, i) &
                                        )
                                transmittance_l2 = exp(-2d0 * (tau_ig - alpha_j * sec(j, l)))
                                transmittance_j = transmittance_j + &
                                    (1d0 - exp(-2d0 * alpha_j * sec(j, l))) * transmittance_l2 * weights_k(ig)
                            end do

                            d_spectral_radius(j, i) = d_spectral_radius(j, i) + area_l(l) * transmittance_j
                        end do
                    end do

                    d_spectral_radius(n_levels, i) = transmittance_l(n_layers)
                    !d_spectral_radius(:, i) = d_spectral_radius(:, i) / sum(d_spectral_radius(:, i)) * &
                    !    (spectral_radius(i) - (target_radius + z(1)) ** 2)

                    d_spectral_radius(:, i) = sqrt(d_spectral_radius(:, i)) * 1d3  ! km to m

                end if

                spectral_radius(i) = sqrt(spectral_radius(i)) * 1d3  ! km to m
            enddo
        end subroutine calculate_transit_spectrum
end module transit_spectrum

