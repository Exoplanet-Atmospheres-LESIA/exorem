module optics
    ! """
    ! Contains useful optics-related functions.
    ! """
    use physics, only: cst_pi, cst_n0

    implicit none

    doubleprecision, parameter :: default_refractive_index = 1.0003d0

    save

    contains
        function get_refractive_index(species, wavenumber) result(n)
            ! """
            ! Return the refractive index of a species at a given wavenumber.
            ! :param species: name of the species
            ! :param wavenumber: (cm-1) wavenumber
            ! :return n: the refractive index of the species
            ! """
            implicit none

            character(len=*), intent(in) :: species
            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n

            if(trim(species) == 'Ar') then
                n = refractive_index_ar(wavenumber)
            else if (trim(species) == 'CH4') then
                n = refractive_index_ch4(wavenumber)
            else if (trim(species) == 'CO') then
                n = refractive_index_co(wavenumber)
            else if (trim(species) == 'CO2') then
                n = refractive_index_co2(wavenumber)
            else if (trim(species) == 'H2') then
                n = refractive_index_h2(wavenumber)
            else if (trim(species) == 'H2O') then
                n = refractive_index_h2o(wavenumber)
            else if (trim(species) == 'He') then
                n = refractive_index_he(wavenumber)
            else if (trim(species) == 'Kr') then
                n = refractive_index_kr(wavenumber)
            else if (trim(species) == 'N2') then
                n = refractive_index_n2(wavenumber)
            else if (trim(species) == 'Ne') then
                n = refractive_index_ne(wavenumber)
            else if (trim(species) == 'NH3') then
                n = refractive_index_nh3(wavenumber)
            else if (trim(species) == 'Xe') then
                n = refractive_index_xe(wavenumber)
            else
                n = default_refractive_index
            end if

            return
        end function get_refractive_index


        function rayleigh_scattering_coefficient(refractive_index, wavenumber) result(sigma)
            ! """
            ! Calculate the Rayleigh scattering coefficient.
            ! Assumed without depolarization term.
            ! Source: http://web.gps.caltech.edu/~vijay/Papers/Rayleigh_Scattering/Bodhaine-etal-99.pdf
            ! :param refractive_index: the refractive index
            ! :param wavenumber: (cm-1) the wavenumber
            ! :return sigma: (cm2) the Rayleigh scattering coefficient
            ! """
            implicit none

            doubleprecision, intent(in) :: refractive_index, wavenumber
            doubleprecision :: sigma
            doubleprecision :: wavelength  ! (m) wavelength

            wavelength = 1d-2 / wavenumber

            sigma = &
                24 * cst_pi ** 3 * (refractive_index ** 2 - 1d0) ** 2 / &
                (wavelength ** 4 * cst_n0 ** 2 * (refractive_index ** 2 + 2) ** 2) * 1d2 ** 2  ! m2 to cm2

            return
        end function rayleigh_scattering_coefficient


        function refractive_index_ar(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of Ar.
            ! Source: Borzsonyi et al. 2008 (https://refractiveindex.info/?shelf=main&book=Ar&page=Borzsonyi)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)

            n = sqrt(&
                    1d0 + &
                    20332.29d-8 * wavelength ** 2 / (wavelength ** 2 - 206.12d-6) + &
                    34458.31d-8 * wavelength ** 2 / (wavelength ** 2 - 8.066d-3) &
                )

            return
        end function refractive_index_ar


        function refractive_index_ch4(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of CH4.
            ! Source: Loria et al. 1909 (https://refractiveindex.info/?shelf=organic&book=methane&page=Loria)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = min(max(1d4 / wavenumber, 0.3d0), 0.6585d0)  ! range of the formulation (slightly extended)

            n = 1.00042607d0 + 6.1396687d-6 * wavelength ** (-2)  ! range of the formulation

            return
        end function refractive_index_ch4


        function refractive_index_co(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of CO.
            ! Source: Sneep et al. 2004 (https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.493.3970&rep=rep1&type=pdf)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.1672d0)  ! range of the formulation

            n = 1d0 + (&
                    22851d0 + &
                    0.456d14 / (71427d0**2 - (1d4 / wavelength)**2)&
                ) * 1e-8

            return
        end function refractive_index_co


        function refractive_index_co2(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of CO2.
            ! Source: Bideau-Mehu et al. 1973 (https://refractiveindex.info/?shelf=main&book=CO2&page=Bideau-Mehu)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(min(1d4 / wavenumber, 1.6945d0), 0.1807d0)  ! range of the formulation

            n = 1d0 + &
                6.99100d-2 / (166.175d0 - wavelength ** (-2)) + &
                1.44720d-3 / (79.609d0 - wavelength ** (-2)) + &
                6.42941d-5 / (56.3064d0 - wavelength ** (-2)) + &
                5.21306d-5 / (46.0196d0 - wavelength ** (-2)) + &
                1.46847d-6 / (0.0584738d0 - wavelength ** (-2))

            return
        end function refractive_index_co2


        function refractive_index_h2(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of H2.
            ! Source: Peck et al. 1977 (https://refractiveindex.info/?shelf=main&book=H2&page=Peck)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.168d0)  ! range of the formulation

            n = 1d0 + &
                0.0148956d0 / (180.7d0 - wavelength ** (-2)) + &
                0.0049037d0 / (92d0 - wavelength ** (-2))

            return
        end function refractive_index_h2


        function refractive_index_h2o(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of H2O at 373.15 K and 1 atm.
            ! Source: IAPWS (http://www.iapws.org/relguide/rindex.pdf)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber

            doubleprecision, parameter :: &
                rho = 8.272535d-1 / 1d3, &  ! H2O(g) density at 373.15 K over standard density ratio
                t = 373.15d0 / 273.15d0, &  ! temperature over standard temperature ratio
                wavelength_0 = 0.589d0, &  ! (um) standard wavelength
                wavelength_uv = 0.2292020d0, &  ! standard wavelength (UV)
                wavelength_ir = 5.432937d0  ! standard wavelength (IR)

            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)
            doubleprecision :: a

            wavelength = max(min(1d4 / wavenumber, 1.1d0), 0.2d0) / wavelength_0  ! range of the formulation

            a = (&
                    0.244257733d0 + &
                    9.74634476d-3 * rho - &
                    3.73234996d-3 * t + &
                    2.68678472d-4 * t * wavelength ** 2 + &
                    1.58920570d-3 / wavelength ** 2 + &
                    2.45934259d-3 / (wavelength ** 2 - wavelength_uv ** 2) + &
                    0.900704920d0 / (wavelength ** 2 - wavelength_ir ** 2) - &
                    1.66626219d-2 * rho ** 2&
                ) * rho

            n = sqrt((2d0 * a + 1) / (1 - a))

            return
        end function refractive_index_h2o


        function refractive_index_he(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of He.
            ! Source: Ermolov et al. 2015 (https://refractiveindex.info/?shelf=main&book=He&page=Ermolov)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.09d0)  ! range of the formulation

            n = sqrt(&
                1d0 + &
                2.16463842d-5 * wavelength ** 2 / (wavelength ** 2 + 6.80769781d-4) + &
                2.10561127d-7 * wavelength ** 2 / (wavelength ** 2 - 5.13251289d-3) + &
                4.75092720d-5 * wavelength ** 2 / (wavelength ** 2 - 3.18621354d-3) &
            )

            return
        end function refractive_index_he


        function refractive_index_kr(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of Kr.
            ! Source: Borzsonyi et al. 2008 (https://refractiveindex.info/?shelf=main&book=Kr&page=Borzsonyi)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)

            n = sqrt(&
                    1d0 + &
                    26102.88d-8 * wavelength ** 2 / (wavelength ** 2 - 2.01d-6) + &
                    56946.82d-8 * wavelength ** 2 / (wavelength ** 2 - 10.043d-3) &
                )

            return
        end function refractive_index_kr


        function refractive_index_n2(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of N2.
            ! Source: Borzsonyi et al. 2008 (https://refractiveindex.info/?shelf=main&book=N2&page=Borzsonyi)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)

            n = sqrt(&
                    1d0 + &
                    39209.95d-8 * wavelength ** 2 / (wavelength ** 2 - 1146.24d-6) + &
                    18806.48d-8 * wavelength ** 2 / (wavelength ** 2 - 13.476d-3) &
                )

            return
        end function refractive_index_n2


        function refractive_index_ne(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of Ne.
            ! Source: Borzsonyi et al. 2008 (https://refractiveindex.info/?shelf=main&book=Ne&page=Borzsonyi)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)

            n = sqrt(&
                    1d0 + &
                    9154.48d-8 * wavelength ** 2 / (wavelength ** 2 - 656.97d-6) + &
                    4018.63d-8 * wavelength ** 2 / (wavelength ** 2 - 5.728d-3) &
                )

            return
        end function refractive_index_ne


        function refractive_index_nh3(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of NH3.
            ! Source: Cuthbertson et al. 1914 (https://refractiveindex.info/?shelf=main&book=NH3&page=Cuthbertson)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)

            n = 1d0 + 0.032953d0 / (90.392d0 - wavelength ** (-2))

            return
        end function refractive_index_nh3


        function refractive_index_xe(wavenumber) result(n)
            ! """
            ! Calculate the refractive index of Xe.
            ! Source: Borzsonyi et al. 2008 (https://refractiveindex.info/?shelf=main&book=Xe&page=Borzsonyi)
            ! :param wavenumber: (cm-1) wavenumebr
            ! :return n: refractive index
            ! """
            implicit none

            doubleprecision, intent(in) :: wavenumber
            doubleprecision :: n
            doubleprecision :: wavelength  ! (um)

            wavelength = max(1d4 / wavenumber, 0.3d0)  ! range of the formulation (slightly extended)
            n = sqrt(&
                    1d0 + &
                    103701.61d-8 * wavelength ** 2 / (wavelength ** 2 - 12750d-6) + &
                    31228.61d-8 * wavelength ** 2 / (wavelength ** 2 - 0.561d-3) &
                )

            return
        end function refractive_index_xe
end module optics